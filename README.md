# Ulak Backend Services

![Ulak](http://icons.iconarchive.com/icons/google/noto-emoji-travel-places/64/42496-school-icon.png)

This is ["Ulak School Management System"] backend service. It is written in Node.js. MongoDB was used as the database. Node.js and MongoDB must be installed to the operation system before "git clone". 

<img src="https://www.guarana-technologies.com/wp-content/uploads/2017/08/node-js-web-app-development.png" width="150" height="150"/><img src="https://cdn-images-1.medium.com/max/800/1*yB8AVO2xadCGmsyTtx0sag.png" width="150" height="150"/><br>

Clone this repo with "git". Then install all dependencies with "npm install" command. Then create an .env file in your project folder and edit it with your private api key as below. And finally, edit /helper/db.js file for your own MongoDB database.


# Settings And Installation
1-) Install all dependencies with "npm install" command. <p>
2-a) Open ".env" file in the project folder for edit. It should be two different api key in ".env" file to sign JWT: First one APISECRETKEY, second one TEMPORARILYTOKENKEY. Because there are two types of jwt in this api for security. The jwt which is signed with TEMPORARILYTOKENKEY is expires in 15 minutes. This jwt must be used only for confirm OTP. And the jwt which is signed with APISECRETKEY is expires in 30 days. This jwt must be used for validate all endpoint under /api/... <br>
2-b) Mail service settings should  be in the ".env" file too. There are three variables for that: <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1-) NODEMAILER_HOST variable is for mail service smtp info. For example: "NODEMAILER_HOST = smtp-mail.outlook.com". <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2-) NODEMAILER_USER variable is for sender email address. For example "NODEMAILER_USER = johndoe@hotmail.com". <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3-) NODEMAILER_PASS variable is for sender email password. <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Briefly ".env" file should look like this: <p> 
<i>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NODEMAILER_HOST = smtp-mail.outlook.com
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NODEMAILER_USER = johndoe@hotmail.com
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NODEMAILER_PASS = FoobarPassword
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;APISECRETKEY = LoremIpsum
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TEMPORARILYTOKENKEY = DolorSitAmet
</i>

<p>3- "/helper/db.js" file should be edited to connect to the MongoDB database. In case of working on the localhost, just open the db.js file and edit "databasename". But if it's wanted to work on a server like "mongolab", change "<i>mongodb://localhost/databasename</i>" with server database connection settings. For example: "<i>mongodb://db_user_name:db_user_password@db.mlab.com:port_number/databasename</i>". 


# Routes-Endpoints

# Index
| Route | HTTP Verb	 | POST body	 | Description	 |
| --- | --- | --- | --- |
| /register | `POST` | { name: 'lorem', surname:'ipsum', email:'dolorsit@amet.com', password:'loremipsum' } | Create a new user.|
| /login | `POST` | { email:'dolorsit@amet.com', password:'loremipsum' }  | Login with email and password. Generates a token.|
| /continuewithfb | `POST` |{ name: 'lorem', surname:'ipsum', facebook_id:'dolor sit amet', password:'loremipsum'}  | Continue with facebook. |
| /givemepasscode | `POST` | { email: 'dolorsit@amet.com'} | Sends a passcode and gives you a temptoken. |


# Users

| Route | HTTP Verb	 | POST body	 | Description	 |
| --- | --- | --- | --- |
| /api/users/updatemyinfo | `PUT` | { 'name':'foo', 'surname':'bar', email: 'dolorsit@amet.com', user_id:'loremipsum',password:'loremipsum' } | Update user with new info. |
| /api/users/changepassword | `PUT` | { password:'loremipsum', newPassword:'loremipsum' user_id:'loremipsum' } | Change user password. |
| /api/users/updatemyphone | `PUT` | { user_id:'loremipsum', phone:'905551234567, password:'loremipsum'} | Add users' phone info. |
| /api/users | `DELETE` | { user_id:'loremipsum', author_id:'authorized person id', author_password:'loremipsum' } | Delete a user(Only for system admins.)|
| /api/users | `DELETE` | { user_id:'loremipsum', author_id:'authorized person id', author_password:'loremipsum' } | Delete a user(Only for system admins.)|
| /lost/verifypasscode | `POST` | { email: 'dolorsit@amet.com', passcode:'loremipsum'} | Verifies passcode and gives you another temptoken. |
| /lost/newpassword | `POST` | { email: 'dolorsit@amet.com', password:'loremipsum'} | Reset user password. |



# Schools

| Route | HTTP Verb	 | POST body	 | Description	 |
| --- | --- | --- | --- |
| /api/schools | `POST` | { user_id:'loremipsum', school_name: 'foobar', state:'lorem', city:'ipsum', school_phone:'02121234567', school_email:'dolorsit@meb.k12.tr', address:'lorem ipsum', category:'lorem ipsum' } | Create a new school. It returns passcode and secure temptoken.|
| /confirmcreation/verifypasscode | `POST` | { user_id:'loremipsum', passcode: 'foobar'} | Confirms school creation.|
| /api/schools | `GET` | { keyword:'loremipsum', state:'lorem', city:'ipsum', category:'lorem ipsum' } | List schools with keyword. |
| /api/schools | `PUT` | { school_id:'loremipsum', user_id:'loremipsum', school_name: 'foobar', state:'lorem', city:'ipsum', school_phone:'02121234567', school_email:'dolorsit@meb.k12.tr', address:'lorem ipsum', category:'lorem ipsum' } | Update school info with school_id. |
| /api/schools | `DELETE` | { school_id:'loremipsum', user_id:'loremipsum'} | Delete a school. It returns  passcode and secure temptoken. |
| /confirmdelete/verifypasscode | `DELETE` | { passcode:'loremipsum', user_id:'loremipsum'} | Delete a school. |

# Classrooms

| Route | HTTP Verb	 | POST body	 | Description	 |
| --- | --- | --- | --- |
| /api/classrooms | `POST` | { user_id:'loremipsum', school_id:'loremipsum' classroom_name: 'foo/bar' } | Create a new classroom. |
| /api/classrooms/getclassrooms | `POST` | {user_id:'loremipsum', school_id:'loremipsum'} | List all classrooms in the school. |
| /api/classrooms | `PUT` | { user_id:'loremipsum', school_id:'loremipsum' classroom_id:'loremipsum', classroom_name: 'foo/bar' } | Update a classroom info with classroom_id. |
| /api/classrooms | `DELETE` |{ user_id:'loremipsum', school_id:'loremipsum' classroom_id:'loremipsum'} | Delete a classroom. |


# Students

| Route | HTTP Verb	 | POST body	 | Description	 |
| --- | --- | --- | --- |
| /api/students| `POST` | { user_id:'loremipsum', name: 'foo', surname:'bar', number:'123', gender:'loremipsum', school_id:'loremipsum' classroom_id:'loremipsum', parent_id:'loremipsum'} | Create a new student. |
| /api/students/getstudentinfo | `POST` | {student_id:'loremipsum', user_id:'loremipsum'} | Get spesific student info. |
| /api/students/getstudentsintheclass | `POST` | {classroom_id:'loremipsum', user_id:'loremipsum'} | List all students in the classrooms. |
| /api/students/search | `POST` | {keyword:'loremipsum', user_id:'loremipsum'}  | Search students with keyword. |
| /api/students | `PUT` | { user_id:'loremipsum', name: 'foo', surname:'bar', number:'123', gender:'loremipsum', school_id:'loremipsum',  classroom_id:'loremipsum', parent_id:'loremipsum'} | Update student with new info. |
| /api/students | `DELETE` | { student_id:'loremipsum', author_id:'dolorsitamet, author_password:'loremipsum' } | Delete a student. |



# Student Info

| Route | HTTP Verb	 | POST body	 | Description	 |
| --- | --- | --- | --- |
| /api/studentinfo | `POST` | { user_id:'loremipsum', student_id:'loremipsum', school_id:'loremipsum', classroom_id:'loremipsum', special_condition:'lorem ipsum dolor sit amet', scores:[{year:'loremipsum', term:'loremipsum', course_name:'lorempisum', exam_result:['lorem', 'ipsum', 'dolor'], oexam_result:['lorem', 'ipsum', 'dolor'], project_result:['lorem', 'ipsum', 'dolor'] }]}| Add info for a student. |
| /api/studentinfo/getinfo | `POST` | { user_id:'loremipsum', student_id:'loremipsum'} | Get a student's info. |
| /api/studentinfo | `PUT` |{ user_id:'loremipsum', student_id:'loremipsum', school_id:'loremipsum', classroom_id:'loremipsum', special_condition:'lorem ipsum dolor sit amet', scores:[{year:'loremipsum', term:'loremipsum', course_name:'lorempisum', exam_results:['lorem', 'ipsum', 'dolor'], oexam_results:['lorem', 'ipsum', 'dolor'], project_results:['lorem', 'ipsum', 'dolor'] }]} | Update a student with new info. |
| /api/studentinfo | `DELETE` |  { user_id:'loremipsum', student_id:'loremipsum'} | Delete a student. |

# Attendances

| Route | HTTP Verb	 | POST body	 | Description	 |
| --- | --- | --- | --- |
| /api/attendances | `POST` | { user_id:'loremipsum', school_id:'loremipsum', classroom_id:'loremipsum', course_name:'loremipsum', period:'loremipsum', date:'dd/mm/yyyy', students_id:['loremipsum', 'loremipsum', 'loremipsum'] } | Create a new attendance record. |
| /api/attendances/getclassbydate | `POST` | { user_id:'loremipsum', school_id:'loremipsum', classroom_id:'loremipsum', date:'dd/mm/yyyy'} | List attendance records of a classrooms by date. |
| /api/attendances/getstudentbydate | `POST` | { user_id:'loremipsum', student_id:'loremipsum', date:'dd/mm/yyyy'} | List attendance records of a student by date. |
| /api/attendances | `PUT` | { record_id:'loremipsum', user_id:'loremipsum', school_id:'loremipsum', classroom_id:'loremipsum', course_name:'loremipsum', period:'loremipsum', date:'dd/mm/yyyy', students_id:['loremipsum', 'loremipsum', 'loremipsum'] } | Update attendance record of a classroom. |
| /api/attendances/updatestudentsinfo | `PUT` | { user_id:'loremipsum', school_id:'loremipsum', classroom_id:'loremipsum', student_id:'loremipsum', course_name:'loremipsum', period:'loremipsum', date:'dd/mm/yyyy', students_id:['loremipsum', 'loremipsum', 'loremipsum'] } | Update attendance record of a classroom. |
| /api/attendances | `DELETE` | { user_id:'loremipsum', school_id:'loremipsum', student_id:'loremipsum', date:'dd/mm/yyyy'} | Delete an attendance record of a student. |

# News (Only For Admins)

| Route | HTTP Verb	 | POST body	 | Description	 |
| --- | --- | --- | --- |
| /api/news | `GET` | Empty | List all news. |
| /api/news | `POST` | { title:'foo', content:'bar', user_id:'loremipsum', picture_url:'picture url', min_auth_level:'12345' } | Create a general news. |
| /api/news/search/:keyword? | `GET` | Empty | Search with keyword in the news. |
| /api/news/:news_id | `GET` | Empty | Get a spesific news with news_id. |
| /api/news | `PUT` | { news_id:'loremipsum', title:'foo', content:'bar', user_id:'loremipsum', picture_url:'picture url', min_auth_level:'12345'} | Update a news with news_id. |
| /api/news | `DELETE` | {news_id:'loremipsum',  user_id:'loremipsum'} | Delete a news with news_id. |


# School Announcements

| Route | HTTP Verb	 | POST body	 | Description	 |
| --- | --- | --- | --- |
| /api/announcements | `GET` | Empty | List all announcements. |
| /api/announcements | `POST` | { title:'foo', content:'bar', user_id:'loremipsum', min_auth_level:'12345' } | Create a school announcements. |
| /api/announcements/search/:keyword? | `GET` | Empty | Search with keyword in the announcements.|
| /api/announcements/:announcement_id | `GET` | Empty | Get a spesific announcements with announcement_id. |
| /api/announcements | `PUT` | { announcement_id:'announcement_id', user_id:'loremipsum', title:'foo', content:'bar', min_auth_level:'12345'} | Update a announcements with announcement_id. |
| /api/announcements | `DELETE` | { announcement_id:'loremipsum',  user_id:'loremipsum' } | Delete an announcements with announcement_id. |

# Test
There is a test line that was added in the "users.js" route. Don't forget to uncomment it before test. Also don't forget the comment it again after the test is finished. This is really important. Attention!! <i>If you forget to comment it again, you have given the OTP(passcode) directly to the user in the response body. This creates a very serious security vulnerability.</i>

#P.S.
Some routes may not be completed. This is an ongoing project. I'm developing this project my free time. 
<p>Enjoy!
