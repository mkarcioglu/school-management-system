const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {type: String,required: true},
    surname: {type: String,required: true},
    facebookId: {type: String, unique:true, sparse:true},
    email: {type:String, unique:true, sparse:true},
    phone: {type:String, unique:true, sparse:true},
    password: {type:String},
    isVerified: {type: Boolean, default:false},
    gender: String,
    birthday: String,
    userInfo: String,
    branch: String,
    joinedSchool: [{
        authorization:{type: Number, default:8},
        school_id: {type: mongoose.Schema.Types.ObjectId},
        class_id: {type: mongoose.Schema.Types.ObjectId},
        student_id: {type:mongoose.Schema.Types.ObjectId},
        licencePlate: String }],
    createdAt:{ type: Date, default: Date.now }
});
module.exports = mongoose.model('user', UserSchema);