const mongoose= require('mongoose');
const Schema= mongoose.Schema;
const NewsSchema=new Schema({
    
    title: {type: String, required:true}, 
    text: {type: String, required:true},
    imagePath: String,
    createdAt: {type:Date, default: Date.now},
    creatorId: {type: mongoose.Types.ObjectId}

});
   

module.exports=mongoose.model('news', NewsSchema);