const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const StudentInfoSchema = new Schema({

    studentName: {type: String,required: true},
    studentSurname: {type: String,required: true},
    studentNumber: {type: Number,required: true},
    school_id: {type: mongoose.Schema.Types.ObjectId,required: true},
    class_id: {type: mongoose.Schema.Types.ObjectId,required: true},
    parent_id: {type: mongoose.Schema.Types.ObjectId},
    specialCondition:{type:String},
    scores:[{
        year: String, 
        term:String, 
        courseName:String,
        examResults:[{type:Number, min:0, max:100}],
        oexamResults:[{type:Number, min:0, max:100}],
        projectResults:[{type:Number, min:0, max:100}]
    }]
});
module.exports = mongoose.model('studentinfo', StudentInfoSchema);