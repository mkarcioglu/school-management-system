const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const JoinRequestSchema = new Schema({

    requester_id: {type: mongoose.Schema.Types.ObjectId, required:true},
    name: {type: String,required: true},
    surname: {type: String,required: true},
    phone: {type:String, required:true},
    email: {type:String},
    schoolEmail: {type:String},
    branch: String,
    licencePlate: String,
    authorization:{type: Number, default:8, required:true},
    school_id: {type: mongoose.Schema.Types.ObjectId, required:true},
    class_id: {type: mongoose.Schema.Types.ObjectId},
    student_id: {type:mongoose.Schema.Types.ObjectId},
    createdAt:{ type: Date, default: Date.now },
    passcode: String,
    isConfirmed: {type: Boolean, default:false}
});
module.exports = mongoose.model('joinrequest', JoinRequestSchema);