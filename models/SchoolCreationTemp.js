const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SchoolCreationTempSchema = new Schema({
    creator_id: {type: mongoose.Schema.Types.ObjectId, unique:true},
    schoolName: {type: String},
    state: {type: String},
    city: String,
    schoolPhone: {type: String, unique:true},
    schoolEmail: {type:String, unique:true},
    fax: String,
    address: String,
    category: String,
    passcode: String,
    createdAt:{ type: Date, default: Date.now }
});
module.exports = mongoose.model('schoolcreation', SchoolCreationTempSchema);