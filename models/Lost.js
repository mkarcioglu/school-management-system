const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LostPasswordSchema = new Schema({
    name: {type: String,required: true},
    surname: {type: String,required: true},
    email: {type:String, required:true, unique:true},
    passcode: String,
    createdAt:{ type: Date, default: Date.now }
});
module.exports = mongoose.model('lostpassword', LostPasswordSchema);
