const mongoose= require('mongoose');
const Schema= mongoose.Schema;
const SchoolSchema=new Schema({

schoolName:{
    type: String,
    required: true,
    maxlength: [60, 'You can enter {VALUE} in {PATH} field. It can not be bigger than {MAXLENGTH}'],

},
state:{
    type: String,
    required: true,
},
city:{
    type: String,
    required: true,
},
schoolCode:{
    type: Number,
    unique:true,
    sparse:true
},
createdAt:{ type: Date, default: Date.now },
addressCode: String,
address: String,
schoolPhone: {type: String, unique:true, sparse:true},
schoolEmail: {type: String, unique:true, sparse:true},
fax: String,
category: String,
creator_id:{type: mongoose.Types.ObjectId, unique:true, sparse:true }
/* wall: [
    {
        announcement:[{
            title: {type: String, required:true}, 
            text: {type: String, required:true},
            createdAt: {type:Date, default: Date.now},
            createdById: {type: mongoose.Types.ObjectId}
        }], 
        news:[{
            title: {type: String, required:true}, 
            text: {type: String, required:true},
            createdAt: {type:Date, default: Date.now},
            createdById: {type: mongoose.Types.ObjectId}
        }]}] */
});


module.exports=mongoose.model('school', SchoolSchema);
