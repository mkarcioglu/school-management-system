const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const StudentSchema = new Schema({

    studentName: {type: String,required: true},
    studentSurname: {type: String,required: true},
    studentNumber: {type: Number,required: true},
    school_id: {type: mongoose.Schema.Types.ObjectId,required: true},
    class_id: {type: mongoose.Schema.Types.ObjectId,required: true},
    parent_id: {type: mongoose.Schema.Types.ObjectId},
    information:{type:String},
    scores:[{
        year: String, // 2018-2019 Education Year
        term:String, //1. Midterm 2. Midterm 
        courseName:String,
        examResults:[{type:Number, min:0, max:100}],
        oexamResults:[{type:Number, min:0, max:100}],
        projectResults:[{type:Number, min:0, max:100}]
    }],
    createdAt:{ type: Date, default: Date.now }
});
module.exports = mongoose.model('student', StudentSchema);