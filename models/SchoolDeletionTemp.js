const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SchoolDeletionTempSchema = new Schema({
    creator_id: {type: mongoose.Schema.Types.ObjectId},
    school_id: {type: mongoose.Schema.Types.ObjectId},
    passcode: String,
    createdAt:{ type: Date, default: Date.now }
});
module.exports = mongoose.model('schooldeletion', SchoolDeletionTempSchema);