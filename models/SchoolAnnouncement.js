const mongoose= require('mongoose');
const Schema= mongoose.Schema;
const AnnouncementSchema=new Schema({

    announcement:[{
        title: {type: String, required:true}, 
        text: {type: String, required:true},
        createdAt: {type:Date, default: Date.now},
        authorization:{type: Number, default:9},
        createdById: {type: mongoose.Types.ObjectId},
        schoolId: {type: mongoose.Schema.Types.ObjectId, required:true},
        classId: {type: mongoose.Schema.Types.ObjectId}
    }],
    
});


module.exports=mongoose.model('announcement', AnnouncementSchema);