const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ClassSchema = new Schema({

    className: {
        type: String,
        required: true,
    },
    school_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    tutor_id: {
        type: mongoose.Schema.Types.ObjectId
    },
    creator_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});


module.exports = mongoose.model('class', ClassSchema);