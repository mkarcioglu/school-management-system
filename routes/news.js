const express = require('express');
const router = express.Router();
const multer = require('multer');

//Models
const UserObject = require('../models/User');
const NewsObject = require('../models/News');

// Send Error Response Give
const sendError = (res, statusCode, message) => {
    res.status(statusCode).json({
        success: false,
        message: message
    });
};

//isAuthorized
const isAuthorized = (user_id, school_id, authorizationLevel) => {
    return new Promise((resolve, reject) => {
        if (user_id == process.env.USER_ID) {
            resolve(true);
        } else {
            const promise = UserObject.findById({
                _id: user_id
            });
            promise.then((data) => {
                console.log(data);
                var auth = false;
                const joinedSchool = data.joinedSchool;
                for (let i = 0; i < joinedSchool.length; i++) {
                    const item = joinedSchool[i];
                    if (school_id == item.school_id) {
                        if (Number(item.authorization) >= authorizationLevel) {
                            auth = true;
                        }
                    }
                }
                resolve(auth);
            }).catch((err) => {
                reject(err);
            });
        }

    });
};

// multer configuration
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, '/home/kochero/Documents/uploads');
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now());
    }
});
var upload = multer({
    storage: storage
});

// Create a news
router.post('/', upload.single('image'), (req, res, next) => {
    const {
        user_id,
        title,
        text
    } = req.body;
    const promise= isAuthorized(user_id, null, null);
    promise.then((auth)=>{
        if(!auth){
            console.log("You are not authorized!");
            sendError(res, 401, "You are not authorized for this process!");
        } else {
            if (!req.file) {
                sendError(res, 400, "No file received!");
            } else {
                const newNews = new NewsObject({
                    creatorId: user_id,
                    title,
                    text,
                    imagePath: '/home/kochero/Documents/uploads/' + req.file.filename
                });
                const promise = newNews.save();
                promise.then((data) => {
                    if (!data) {
                        sendError(res, 500, "Something went wrong! Try again later!");
                    } else {
                        res.status(200).json({
                            success: true,
                            message: "Image succesfully uploaded!"
                        });
                    }
                }).catch((err) => {
                    sendError(res, 500, "Something went wrong! Try again later!");
                });
            }
        }
    }).catch((err)=>{
        console.log("You are not authorized!");
        sendError(res, 401, "You are not authorized for this process!");
    });
});


module.exports = router;