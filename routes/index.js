const express = require('express');
const router = express.Router();

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');


//Models
const UserObject = require('../models/User');
const LostObject = require('../models/Lost');
const NewsObject = require('../models/News');



// GET home page.
router.get('/', (req, res, next) => {
  res.status(200).json({
    success:true,
    message: "Welcome"
  });
});
// GET home page.
router.post('/', (req, res, next) => {
  res.status(200).json({
    success:true,
    message: "Welcome"
  });
});

router.post('/register', (req, res, next) => {

  const {
    name,
    surname,
    email,
    password
  } = req.body;
  bcrypt.hash(password, 10).then((hash) => {
    const user = new UserObject({
      name,
      surname,
      email,
      password: hash
    });
    const promise = user.save();
    promise.then((data) => {
      res.status(200).json({
        success: true,
        message: 'Success!'
      });
    }).catch((err) => {
      console.log(err);

      const response = {
        success: false,
        errorName: err.name,
        errorCode: err.code,
        errorCodeName: err.codeName,
        message: err.errmsg
      };
      if (err.code == 11000) {
        res.status(403).json({
          success: false,
          message: 'Dublicate key error!'
        });
      } else {
        res.status(403).json(response);
      }

    });
  });

});

router.post('/login', (req, res, next) => {

  const {
    email,
    password
  } = req.body;

  const promise = UserObject.findOne({
    email
  });
  promise.then((user) => {
    if (!user) {

      res.status(404).json({
        success: false,
        message: "Authentication failed. User not found!"
      });
    } else {

      const user_id = user._id;
      const info = {
        user_id: user._id,
        name: user.name,
        surname: user.surname,
        isVerified: user.isVerified,
        joinedSchool: user.joinedSchool
      };

      bcrypt.compare(password, user.password).then((result) => {
        if (!result) {
          res.status(404).json({
            success: false,
            message: "Authentication failed. Wrong password!"
          });
        } else {
          const payload = {
            user_id
          };
          const token = jwt.sign(payload, process.env.APISECRETKEY, {
            expiresIn: 86400 * 14 
          });

          info.token = token;
          info.success = true;
          info.message = "Success!";
          res.status(200).json(info);
        }
      }).catch((err) => {
        console.log(err);
        res.status(500).json({
          success: false,
          message: "Internal server error!"
        });
      });
    }
  }).catch((err) => {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong. Try again later."
    });
  });
});

router.post('/continuewithfb', (req, res, next) => {
  const {
    name,
    surname,
    password,
    facebook_id
  } = req.body;
  const facebookId = facebook_id;

  const promise = UserObject.findOne({
    facebookId
  });
  promise.then((user) => {
    if(!user){
      if(req.body.password){
        bcrypt.hash(password, 10).then((hash) => {
          const newUser = new UserObject({
            name,
            surname,
            facebookId: facebook_id,
            password: hash
          });
          const promise = newUser.save();
          promise.then((user) => {
            const _id = user._id;
          const info = {
            _id: user._id,
            name: user.name,
            surname: user.surname,
            isVerified: user.isVerified,
            joinedSchool: user.joinedSchool
          };
          const payload = {
            _id
          };
          const token = jwt.sign(payload, process.env.APISECRETKEY, {
            expiresIn: 86400 * 14 
          });
          info.success = true;
          info.token = token;
          info.message = "Success!";
          res.status(200).json(info);
          }).catch((err) => {
            console.log(err);
            res.status(500).json({
              success: false,
              message: "Internal server error!"
            });      
          });
        });
      }else{
          res.status(406).json({
              success: false,
              message: "Doesn't have a password yet!"
          });
      }
  }else {

      const _id = user._id;
      const info = {
        _id: user._id,
        name: user.name,
        surname: user.surname,
        isVerified: user.isVerified,
        joinedSchool: user.joinedSchool
      };
      const payload = {
        _id
      };
      const token = jwt.sign(payload, process.env.APISECRETKEY, {
        expiresIn: 86400 * 14 
      });
      info.success = true;
      info.token = token;
      info.message = "Success!";
      res.status(200).json(info);
    }

  }).catch((err) => {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong. Try again later."
    });
  });
});

router.post('/givemepasscode', (req, res, next) => {
  const {
    email
  } = req.body;
  const promise = UserObject.findOne({
    email
  });
  promise.then((user) => {
    if (!user) {
      console.log("User not found and we lied!");
      res.status(200).json({
        success: true,
        message: "If your address is correct, you will receive an email!"
      });
    } else {
      const receiver = {
        name: user.name,
        surname: user.surname
      };
      const num = Math.floor(Math.random() * (999999 - 100000)) + 100000;
      const passcode = num.toString();
      bcrypt.hash(passcode, 10).then((hash) => {

        const promise = LostObject.updateOne({
          email
        }, {
          name: user.name,
          surname: user.surname,
          email: user.email,
          passcode: hash
        }, {
          upsert: true
        });
        promise.then((data) => {
          //res.json(data);
          console.log("Db kayıt başarılı!");
          console.log(data);
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Something went wrong. Try again later."
          });
        });
      });

      let transporter = nodemailer.createTransport({
        host: process.env.NODEMAILER_HOST,
        auth: {
          user: process.env.NODEMAILER_USER,
          pass: process.env.NODEMAILER_PASS
        }
      });

      let mailOptions = {
        from: process.env.NODEMAILER_USER,
        to: email,
        subject: passcode + ' Ulak Şifre Sıfırlama Kodu',
        text: "Sayın " + receiver.name + " " + receiver.surname + "\n\nUlak şifre sıfırlama kodunuz: " + passcode
      };

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          res.status(500).json({
            success: false,
            message: "Something went wrong. Try again later."
          });
          return console.log(error);
        } else {
          console.log('Message sent: %s', info.messageId);
          // Preview only available when sending through an Ethereal account
          console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

          console.log("Passcode:" + passcode);

          const payload = {
            email: user.email
          };
          const temptoken = jwt.sign(payload, process.env.TEMPORARILYTOKENKEY, {
            expiresIn: 300
          });

          res.status(200).json({
            success: true,
            message: "If your address is correct, you will receive an email!",
            temptoken: temptoken,
            passcode: passcode
            // IMPORTANT: THIS IS OTP TEST LINE. YOU HAVE TO COMMENT OUT WHEN YOU FINISH YOUR TEST
          });

        }

      });

      //**************************************************** */



    } 
  }).catch((err) => {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong. Try again later."
    });
  });
});

// Get last news to ten by ten
router.get('/', (req, res, next) => {
 const promise= NewsObject.aggregate()
});

module.exports = router;