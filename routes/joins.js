const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const mongoose = require('mongoose');


// Models
const SchoolObject = require('../models/School');
const UserObject = require('../models/User');
const JoinRequestObject = require('../models/JoinRequest');
const StudentObject = require('../models/Student');

//Acceptable Check
const isAcceptable = (user_id, school_id, student_id, authorization, limit) => {
  return new Promise((resolve, reject) => {
    const promise = UserObject.findById({
      _id: user_id
    });
    promise.then((data) => {
      let condition = true;
      let cause = "";
      let count = 0;
      const joinedSchool = data.joinedSchool;
      for (let i = 0; i < joinedSchool.length; i++) {
        const item = joinedSchool[i];
        //If user wants to join same school with same auth, lets check that.
        if (school_id.toString() == item.school_id && authorization.toString() == item.authorization) {
          if (authorization.toString() == "13" || authorization.toString() == "12") {
            condition = false;
            cause = "Duplication";
          } else if (authorization.toString() == "11") {
            if (student_id != item.student_id) {
              condition = true;
            } else {
              condition = false;
              cause = "Duplication";
            }
          } else if (authorization.toString() == "10") {
            condition = false;
            cause = "Duplication";
          } else if (authorization.toString() == "9") {
            condition = false;
            cause = "Duplication";
          }
        }
        if (authorization.toString() == item.authorization) {
          count++;
          if (count > (limit)) {
            condition = false;
            cause = "Limitation";
          }
        }
      }
      const promise = JoinRequestObject.countDocuments({
        requester_id: user_id
      });
      promise.then(requestCount => {
        if (requestCount > 4) {
          condition = false;
          cause = "Too many join requests";
        }
        resolve([condition, cause]);
      }).catch((err) => {
        reject(err);
      });

    }).catch((err) => {
      reject(err);
    });
  });
};

//Authorization Check
const isAuth = (user_id, school_id, authorization) => {
  return new Promise((resolve, reject) => {
    const promise = UserObject.findById({
      _id: user_id
    });
    promise.then((data) => {
      let auth = false;
      const joinedSchool = data.joinedSchool;
      for (let i = 0; i < joinedSchool.length; i++) {
        const item = joinedSchool[i];
        if (school_id.toString() == item.school_id && authorization.toString() == item.authorization) {
          auth = true;
        }
      }
      resolve(auth);
    }).catch((err) => {
      reject(err);
    });
  });
};

// Send Error Response
const sendError = (res, statusCode, message) => {
  res.status(statusCode).json({
    success: false,
    message: message
  });
};

// Approves joins
const confirmJoining = (req, res, data) => {
  const {
    school_id,
    request_id,
    student_id,
    class_id,
    licencePlate
  } = req.body;
  const _id = data.requester_id;

  switch (data.authorization) {
    case 12:
      // Teacher object registration
      const promise = UserObject.findByIdAndUpdate(
        _id, {
          "$addToSet": {
            joinedSchool: {
              authorization: 12,
              school_id: school_id
            }
          }
        }, {
          new: true
        });
      promise.then((data) => {
        if (!data) {
          sendError(res, 500, "Something went wrong! Try again later!");
        } else {
          const promise = JoinRequestObject.findOneAndUpdate({
            _id: request_id
          }, {
            isConfirmed: true
          }, {
            upsert: true
          });
          // ## upsert:true parameter will be remove. Its added isConfirmed to db.
          promise.then((data) => {
            res.status(200).json({
              success: true,
              message: "Confirmed!"
            });
          }).catch((err) => {
            console.log(err);
            res.status(500).json({
              success: false,
              message: "Something went wrong! Try again later!"
            });
          });
        }
      }).catch((err) => {
        console.log(err);
        res.status(500).json({
          success: false,
          message: "Something went wrong! Try again later!"
        });
      });
      break;
    case 11:
      // Parent object registration.
      if (req.body.student_id == undefined || req.body.class_id == undefined) {
        sendError(res, 406, "You have sent missing or incorrect information!");
      } else {
        const promise = UserObject.findByIdAndUpdate(
          _id, {
            "$addToSet": {
              joinedSchool: {
                authorization: 11,
                school_id: school_id,
                student_id: student_id,
                class_id: class_id
              }
            }
          }, {
            new: true
          });
        promise.then((data) => {
          if (!data) {
            sendError(res, 500, "Something went wrong! Try again later!");
          } else {
            const promise = StudentObject.findOneAndUpdate({
              _id: student_id
            }, {
              parent_id: _id
            });
            promise.then((data) => {
              if (!data) {
                sendError(res, 500, "Something went wrong! Try again later!");
              } else {
                const promise = JoinRequestObject.findOneAndUpdate({
                  _id: request_id
                }, {
                  isConfirmed: true
                }, {
                  upsert: true
                });
                //## This upsert will be removed too.
                promise.then((data) => {
                  res.status(200).json({
                    success: true,
                    message: "Confirmed!"
                  });
                }).catch((err) => {
                  console.log(err);
                  sendError(res, 500, "Something went wrong! Try again later!");
                });
              }
            }).catch((err) => {
              console.log(err);
              sendError(res, 500, "Something went wrong! Try again later!");
            });
          }
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Something went wrong! Try again later!"
          });
        });
      }
      break;
    case 10:
      // Student object registration.
      if (req.body.student_id == undefined || req.body.class_id == undefined) {
        sendError(res, 406, "You have sent missing or incorrect information!");
      } else {
        const promise = UserObject.findByIdAndUpdate(
          _id, {
            "$addToSet": {
              joinedSchool: {
                authorization: 10,
                school_id: school_id,
                class_id: class_id
              }
            }
          }, {
            new: true
          });
        promise.then((data) => {
          if (!data) {
            sendError(res, 500, "Something went wrong! Try again later!");
          } else {
            const promise = JoinRequestObject.findOneAndUpdate({
              _id: request_id
            }, {
              isConfirmed: true
            }, {
              upsert: true
            });
            // ## This is too.
            promise.then((data) => {
              res.status(200).json({
                success: true,
                message: "Confirmed!"
              });
            }).catch((err) => {
              console.log(err);
              res.status(500).json({
                success: false,
                message: "Something went wrong! Try again later!"
              });
            });
          }
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Something went wrong! Try again later!"
          });
        });
      }
      break;
    case 9:
      if (req.body.licencePlate == undefined) {
        sendError(res, 406, "You have sent missing or incorrect information!");
      } else {
        const promise = UserObject.findByIdAndUpdate(
          _id, {
            "$addToSet": {
              joinedSchool: {
                authorization: 9,
                school_id: school_id,
                licencePlate: licencePlate
              }
            }
          }, {
            new: true
          });
        promise.then((data) => {
          if (!data) {
            sendError(res, 500, "Something went wrong! Try again later!");
          } else {
            const promise = JoinRequestObject.findOneAndUpdate({
              _id: request_id
            }, {
              isConfirmed: true
            }, {
              upsert: true
            });
            // ## You got it.
            promise.then((data) => {
              res.status(200).json({
                success: true,
                message: "Confirmed!"
              });
            }).catch((err) => {
              console.log(err);
              res.status(500).json({
                success: false,
                message: "Something went wrong! Try again later!"
              });
            });
          }
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Something went wrong! Try again later!"
          });
        });

      }
      break;
    default:
      sendError(res, 500, "Something went wrong! Try again later!");
      break;
  }
};

const cancelJoinRequest=(res, request_id)=>{
const promise=JoinRequestObject.findByIdAndDelete({_id:request_id});
promise.then((data)=>{
if(!data){
  sendError(res, 500, "Something went wrong! Try again later! 2");
} else {
  res.status(200).json({
    success:true,
    message:"Request deleted!"
  });
}
}).catch((err)=>{
  console.log(err);
  sendError(res, 500, "Something went wrong! Try again later!");
});
};

// Join As Director
router.post('/joinasdirector', (req, res, next) => {
  const {
    user_id,
    schoolEmail,
    school_id
  } = req.body;
  const authorization = 13;
  const phone = req.phone;
  const name = req.name;
  const surname = req.surname;

  // isAcceptable starts
  const limit = 3;
  // limit means, users can join as director only 3 schools.  
  const student_id = null;
  const promise = isAcceptable(user_id, school_id, student_id, authorization, limit);
  promise.then(([condition, cause]) => {
    if (!condition) {
      res.status(406).json({
        success: false,
        message: cause + " error!"
      });
    } else {
      if (!schoolEmail) {
        res.status(406).json({
          success: false,
          message: "A valid school email address required!"
        });
      } else {
        //If you use another email system you can use regex for confirmation.
        const sp = schoolEmail.split('@');
        if (sp[1] != 'meb.k12.tr') {
          res.status(406).json({
            success: false,
            message: "A valid school email address required!"
          });
        } else {
          const num = Math.floor(Math.random() * (999999 - 100000)) + 100000;
          const passcode = num.toString();

          bcrypt.hash(passcode, 10).then((hash) => {
            const promise = JoinRequestObject.updateOne({
              requester_id: user_id
            }, {
              requester_id: user_id,
              school_id: school_id,
              schoolEmail: schoolEmail,
              authorization: authorization,
              phone: phone,
              name: name,
              surname: surname,
              passcode: hash
            }, {
              upsert: true,
              new: true
            });

            promise.then((data) => {

              const _id = mongoose.Types.ObjectId(user_id);
              const promise = UserObject.findById({
                _id
              });
              promise.then((user) => {
                const receiver = {
                  name: user.name,
                  surname: user.surname
                };
                let transporter = nodemailer.createTransport({
                  host: process.env.NODEMAILER_HOST,
                  auth: {
                    user: process.env.NODEMAILER_USER,
                    pass: process.env.NODEMAILER_PASS
                  }
                });

                let mailOptions = {
                  from: process.env.NODEMAILER_USER,
                  to: schoolEmail,
                  subject: passcode + ' Ulak İşlem Kodu',
                  text: "Sayın " + receiver.name + " " + receiver.surname + "\n\nUlak Okul Sistemlerine müdür olarak bağlanabilmeniz için altı haneli kodunuz: " + passcode
                };

                transporter.sendMail(mailOptions, (error, info) => {
                  if (error) {
                    res.status(500).json({
                      success: false,
                      message: "Something went wrong. Try again later."
                    });
                    return console.log(error);
                  } else {
                    console.log('Message sent: %s', info.messageId);
                    // Preview only available when sending through an Ethereal account
                    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                    const payload = {
                      user_id
                    };
                    const temptoken = jwt.sign(payload, process.env.TEMPORARILYTOKENKEY, {
                      expiresIn: 900
                    });
                    res.status(200).json({
                      success: true,
                      message: "If your address is correct, you will receive an email!",
                      temptoken: temptoken,
                      passcode: passcode
                      // IMPORTANT: THIS IS OTP TEST LINE. YOU HAVE TO COMMENT OUT WHEN YOU FINISH YOUR TEST
                    });

                  }
                });

              }).catch((err) => {
                console.log(err);
                res.status(500).json({
                  success: false,
                  message: "Something went wrong! Try again later!"
                });
              });
            }).catch((err) => {
              console.log(err);
              res.status(500).json({
                success: false,
                message: "Something went wrong. Try again later."
              });
            });
          });
        }
      }
    }
  }).catch((err) => {
    console.log(err);
    sendError(res, 500, "Something went wrong. Try again later!");
  });
  // isAcceptable ends
});

//Join As Teacher
router.post('/joinasteacher', (req, res, next) => {
  const {
    user_id,
    school_id,
    branch
  } = req.body;
  const authorization = 12;
  const phone = req.phone;
  const name = req.name;
  const surname = req.surname;

  // isAcceptable starts
  const limit = 3;
  // limit means, users can join as teacher only 3 schools.
  const student_id = null;
  const promise = isAcceptable(user_id, school_id, student_id, authorization, limit);
  promise.then(([condition, cause]) => {
    if (!condition) {
      res.status(406).json({
        success: false,
        message: cause + " error!"
      });
    } else {
      const newRequest = new JoinRequestObject({
        requester_id: user_id,
        school_id: school_id,
        authorization: authorization,
        name: name,
        surname: surname,
        phone: phone,
        branch: branch
      });
      const promise = newRequest.save();
      promise.then((data) => {
        res.status(200).json({
          success: true,
          message: "Join request created! Please contact your school director!"
        });
      }).catch((err) => {
        console.log(err);
        res.status(500).json({
          success: false,
          message: "Something went wrong! Try again later!"
        });
      });
    }
  }).catch((err) => {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong! Try again later!"
    });

  });
  // isAcceptable ends
});

// Join As Parents
router.post('/joinasparent', (req, res, next) => {
  const {
    user_id,
    school_id,
    studentNumber
  } = req.body;
  const authorization = 11;
  const phone = req.phone;
  const name = req.name;
  const surname = req.surname;
  // Process Start
  const promise = StudentObject.countDocuments({
    $and: [{
      school_id: school_id
    }, {
      studentNumber: studentNumber
    }]
  });
  promise.then((count) => {
    if (count == 0) {
      res.status(404).json({
        success: false,
        message: "Student not found!"
      });
    } else if (count == 1) {
      const promise = StudentObject.findOne({
        $and: [{
          school_id: school_id
        }, {
          studentNumber: studentNumber
        }]
      });
      promise.then((data) => {
        // isAcceptable starts
        const limit = 5;
        const student_id = data._id;
        // limit means, users can join as parents only 5 schools.
        const promise = isAcceptable(user_id, school_id, student_id, authorization, limit);
        promise.then(([condition, cause]) => {
          if (!condition) {
            res.status(406).json({
              success: false,
              message: cause + " error!"
            });
          } else {
            const newRequest = new JoinRequestObject({
              requester_id: user_id,
              school_id: school_id,
              student_id: student_id,
              authorization: authorization,
              name: name,
              surname: surname,
              phone: phone,
            });
            const promise = newRequest.save();
            promise.then((data) => {
              res.status(200).json({
                success: true,
                message: "Join request created! Please contact your school director!"
              });
            }).catch((err) => {
              console.log(err);
              res.status(500).json({
                success: false,
                message: "Something went wrong! Try again later! 1"
              });
            });
          }
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Something went wrong! Try again later! 2 "
          });

        });
        // isAcceptable ends
      }).catch((err) => {
        console.log(err);
        res.status(500).json({
          success: false,
          message: "Something went wrong! Try again later! 3"
        });
      });
    } else {
      res.status(500).json({
        success: false,
        message: "Something went wrong! Try again later! 4"
      });
    }
  }).catch((err) => {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong! Try again later! 5"
    });
  }); // Process End  
});

// Join As Students
router.post('/joinasstudent', (req, res, next) => {
  const {
    user_id,
    school_id,
    studentNumber
  } = req.body;
  const authorization = 10;
  const phone = req.phone;
  const name = req.name;
  const surname = req.surname;
  // Process Start
  const promise = StudentObject.countDocuments({
    $and: [{
      school_id: school_id
    }, {
      studentNumber: studentNumber
    }]
  });
  promise.then((count) => {
    if (count == 0) {
      res.status(404).json({
        success: false,
        message: "Student not found!"
      });
    } else if (count == 1) {
      const promise = StudentObject.findOne({
        $and: [{
          school_id: school_id
        }, {
          studentNumber: studentNumber
        }]
      });
      promise.then((data) => {
        // isAcceptable starts
        const limit = 1;
        const student_id = data._id;
        // limit means, users can join as students only 1 schools.
        const promise = isAcceptable(user_id, school_id, student_id, authorization, limit);
        promise.then(([condition, cause]) => {
          if (!condition) {
            res.status(406).json({
              success: false,
              message: cause + " error!"
            });
          } else {
            const newRequest = new JoinRequestObject({
              requester_id: user_id,
              school_id: school_id,
              student_id: student_id,
              authorization: authorization,
              name: name,
              surname: surname,
              phone: phone,
            });
            const promise = newRequest.save();
            promise.then((data) => {
              res.status(200).json({
                success: true,
                message: "Join request created! Please contact your school director or tutor!"
              });
            }).catch((err) => {
              console.log(err);
              res.status(500).json({
                success: false,
                message: "Something went wrong! Try again later! 1"
              });
            });
          }
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Something went wrong! Try again later! 2 "
          });

        });
        // isAcceptable ends
      }).catch((err) => {
        console.log(err);
        res.status(500).json({
          success: false,
          message: "Something went wrong! Try again later! 3"
        });
      });
    } else {
      res.status(500).json({
        success: false,
        message: "Something went wrong! Try again later! 4"
      });
    }
  }).catch((err) => {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong! Try again later! 5"
    });
  }); // Process End  
});

// Join As bus driver
router.post('/joinasbusdriver', (req, res, next) => {
  const {
    user_id,
    school_id,
    licencePlate
  } = req.body;
  const authorization = 9;
  const phone = req.phone;
  const name = req.name;
  const surname = req.surname;
  // Process Start
  // isAcceptable starts
  const limit = 5;
  const student_id = null;
  // limit means, users can join as bus driver only 5 schools.
  const promise = isAcceptable(user_id, school_id, student_id, authorization, limit);
  promise.then(([condition, cause]) => {
    if (!condition) {
      res.status(406).json({
        success: false,
        message: cause + " error!"
      });
    } else {
      const newRequest = new JoinRequestObject({
        requester_id: user_id,
        school_id: school_id,
        licencePlate: licencePlate,
        authorization: authorization,
        name: name,
        surname: surname,
        phone: phone,
      });
      const promise = newRequest.save();
      promise.then((data) => {
        res.status(200).json({
          success: true,
          message: "Join request created! Please contact your school director!"
        });
      }).catch((err) => {
        console.log(err);
        res.status(500).json({
          success: false,
          message: "Something went wrong! Try again later! 1"
        });
      });
    }
  }).catch((err) => {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong! Try again later! 2 "
    });

  });
  // isAcceptable ends
  // Process ends
});

// Join As Follower.
router.post('/joinasfollower', (req, res, next) => {
  const {
    user_id,
    school_id
  } = req.body;
  const authorization = 8;
  const phone = req.phone;
  const name = req.name;
  const surname = req.surname;
  // Process Start
  // isAcceptable starts
  const limit = 10;
  const student_id = null;
  // limit means, users can join as follower only 10 schools.
  const promise = isAcceptable(user_id, school_id, student_id, authorization, limit);
  promise.then(([condition, cause]) => {
    if (!condition) {
      res.status(406).json({
        success: false,
        message: cause + " error!"
      });
    } else {
      // Follower object registration.
      const promise = UserObject.findByIdAndUpdate(
        _id, {
          "$addToSet": {
            joinedSchool: {
              authorization: 8,
              school_id: school_id
            }
          }
        }, {
          new: true
        });
      promise.then((data) => {
        if (!data) {
          sendError(res, 500, "Something went wrong! Try again later!");
        } else {
          res.status(200).json({
            success: true,
            message: "Success!"
          });
        }
      }).catch((err) => {
        console.log(err);
        res.status(500).json({
          success: false,
          message: "Something went wrong! Try again later!"
        });
      });
    }
  }).catch((err) => {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong! Try again later! 2 "
    });

  });
  // isAcceptable ends
  // Process ends
});

// Read - Gets School Join Requests
router.get('/schooljoinrequests', (req, res, next) => {
  const {
    user_id,
    school_id
  } = req.query;
  if (req.query.user_id == undefined || req.query.school_id == undefined) {
    sendError(res, 406, "You have sent missing or incorrect information!");
  } else {
    if (user_id != req.user_id) {
      sendError(res, 403, "Forbidden! ID match error!");
    } else {
      const promise = isAuth(user_id, school_id, 13);
      promise.then((auth) => {
        if (!auth) {
          sendError(res, 401, "You are not authorized!");
        } else {
          const promise = JoinRequestObject.aggregate([{
              $match: {
                school_id: mongoose.Types.ObjectId(school_id),
                isConfirmed: false
              }
            },
            {
              $lookup: {
                from: 'students',
                localField: 'student_id',
                foreignField: '_id',
                as: 'student'
              }
            },
            {
              $unwind: {
                path: '$student',
                preserveNullAndEmptyArrays: true
              }
            },
            {
              $lookup: {
                from: 'classes',
                localField: 'student.class_id',
                foreignField: '_id',
                as: 'class'
              }
            },
            {
              $unwind: {
                path: '$class',
                preserveNullAndEmptyArrays: true
              }
            },
            {
              $project: {
                _id: 1,
                authorization: 1,
                name: 1,
                surname: 1,
                phone: 1,
                requester_id: 1,
                school_id: 1,
                branch: 1,
                licencePlate: 1,
                studentName: '$student.studentName',
                studentSurname: '$student.studentSurname',
                studentNumber: '$student.studentNumber',
                student_id: '$student._id',
                className: '$class.className',
                class_id: '$class._id'
              }
            }
          ]);
          promise.then((data) => {
            res.status(200).json(data);
          }).catch((err) => {
            console.log(err);
            sendError(res, 500, "Something went wrong! Try again later!");
          });
        }
      }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
      });
    }
  }
});

// Confirm all type of join requests
router.post('/confirm', (req, res, next) => {
  const {
    user_id,
    school_id,
    request_id,
    student_id,
    class_id,
    licencePlate,
    passcode
  } = req.body;
  if (req.body.user_id == undefined || req.body.request_id == undefined || req.body.school_id == undefined) {
    sendError(res, 406, "You have sent missing or incorrect information!");
  } else {
      const promise = JoinRequestObject.findOne({
        _id: request_id
      });
      promise.then((data) => {
        if (!data) {
          sendError(res, 500, "Something went wrong! Try again later!");
        } else if(data.isConfirmed) {
          sendError(res, 406, "Repetitive entry! It has already been approved!");
        } else {
          const promise = isAuth(user_id, data.school_id, 13);
          promise.then((auth) => {
            if (auth) {
              confirmJoining(req, res, data);
            } else if (req.body.passcode != undefined) {
              bcrypt.compare(passcode, data.passcode).then((result) => {
                if (!result) {
                  sendError(res, 403, "Wrong ID or passcode!");
                } else {
                  confirmJoining(req, res, data);
                }
              }).catch((err) => {
                console.log(err);
                sendError(res, 500, "Something went wrong! Try again later!");
              });
            } else {
              sendError(res, 401, "You are not authorized!");
            }
          }).catch((err) => {
            console.log(err);
            sendError(res, 500, "Something went wrong! Try again later!");
          });
        }
      }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
      });
  }

});

// Get passcode for membership requests
router.post('/getpasscode', (req, res, next) => {
  const {
    user_id,
    request_id,
  } = req.body;
  if (req.body.user_id == undefined || req.body.request_id == undefined) {
    sendError(res, 406, "You have sent missing or incorrect information!");
  } else {
    if (user_id != req.user_id) {
      sendError(res, 403, "Forbidden! ID match error!");
    } else {
      const promise = JoinRequestObject.findOne({
        _id: request_id
      });
      promise.then((data) => {
        if (!data) {
          sendError(res, 500, "Something went wrong! Try again later!");
        } else {
          const promise = isAuth(user_id, data.school_id, 13);
          promise.then((auth) => {
            if (!auth) {
              sendError(res, 401, "You are not authorized!");
            } else {
              // Get passcode
              const num = Math.floor(Math.random() * (999999 - 100000)) + 100000;
              const passcode = num.toString();
              bcrypt.hash(passcode, 10).then((hash) => {
                const promise = JoinRequestObject.findOneAndUpdate({
                  _id: request_id
                }, {
                  passcode: hash
                }, {
                  upsert: true
                });
                promise.then((data) => {
                  res.status(200).json({
                    success: true,
                    message: "Successful!",
                    passcode: passcode
                  });
                }).catch((err) => {
                  console.log(err);
                  res.status(500).json({
                    success: false,
                    message: "Something went wrong! Try again later!"
                  });
                });
              });
            }
          }).catch((err) => {
            console.log(err);
            sendError(res, 500, "Something went wrong! Try again later!");
          });
        }
      }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
      });
    }
  }
});

/* // Verifies passcode
router.post('/verifypasscode', (req, res, next) => {


  const {
    user_id,
    request_id,
    passcode
  } = req.body;
  if (user_id != req.user_id) {
    sendError(res, 403, "Forbidden! ID mismatch!");
  } else {
    const promise = JoinRequestObject.findById({
      _id: request_id
    });
    promise.then((data) => {
      bcrypt.compare(passcode, data.passcode).then((result) => {
        if (!result) {
          sendError(res, 403, "Wrong ID or passcode!");
        } else {

        }
      }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
      });

    }).catch((err) => {
      console.log(err);
      sendError(res, 500, "Something went wrong! Try again later!");
    });
  }

}); */

// Reject join requests
router.post('/reject', (req, res, next) => {
  const {
    user_id,
    request_id,
  } = req.body;
  if (req.body.user_id == undefined || req.body.request_id == undefined) {
    sendError(res, 406, "You have sent missing or incorrect information!");
  } else {
    if (user_id != req.user_id) {
      sendError(res, 403, "Forbidden! ID match error!");
    } else {
      const promise = JoinRequestObject.findOne({
        _id: request_id
      });
      promise.then((data) => {
        if (!data) {
          sendError(res, 500, "Something went wrong! Try again later!");
        } else if(data.isConfirmed) {
          sendError(res, 406, "Repetitive entry! It has already been approved!");
        } else {
          const promise = isAuth(user_id, data.school_id, 13);
          promise.then((auth) => {
            if (auth) {
              cancelJoinRequest(res, request_id);
            } else if (user_id == data.requester_id) {
              cancelJoinRequest(res, request_id);
            } else {
              sendError(res, 401, "You are not authorized!");
            }
          }).catch((err) => {
            console.log(err);
            sendError(res, 500, "Something went wrong! Try again later!");
          });
        }
      }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
      });
    } 
  }

});

module.exports = router;