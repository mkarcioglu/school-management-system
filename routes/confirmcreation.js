const express = require('express');
const router = express.Router();

const bcrypt = require('bcryptjs');

const SchoolObject = require('../models/School');
const SchoolCreationTempObject = require('../models/SchoolCreationTemp');
const UserObject = require('../models/User');

router.post('/verifypasscode', (req, res, next)=>{
    const passcode=req.body.passcode;
    const user_id=req.user_id;
    const promise=SchoolCreationTempObject.findOne({
        creator_id:user_id
    });
    promise.then((data)=>{
        console.log(data);
        
        bcrypt.compare(passcode, data.passcode).then((result) => {
            console.log(result);
  
            // This result is true or false
            if (!result) {
              // If passcode(otp) is wrong, lets send error message 
              res.status(404).json({
                success: false,
                message: "Authentication failed. Wrong passcode!"
              });
            } else {
              // If passcode is true, lets do something

              const school = new SchoolObject({
                creator_id: user_id,
                schoolName:data.schoolName, 
                state:data.state,
                city:data.city,
                schoolPhone:data.schoolPhone,
                fax:data.fax,
                address:data.address,
                category:data.category,
                schoolEmail:data.schoolEmail
              });
              const promise=school.save();
              promise.then((data)=>{
                if(!data){
                    res.status(500).json({
                        success:false,
                        message:"Something went wrong! Try again later!"
                    });
                } else{
                     const info={
                      school_id:data._id,
                      schoolName:data.schoolName,
                      state:data.state,
                      city:data.city,
                      schoolPhone:data.schoolPhone,
                      schoolEmail:data.schoolEmail,
                      fax:data.fax,
                      address:data.address,
                      category:data.category
                    };

                    const promise = UserObject.findByIdAndUpdate(
                      user_id, {
                        "$addToSet": {
                          joinedSchool: {
                            authorization: 13,
                            school_id: data._id
                          }
                        }
                      }, {
                        new: true
                      });
                      promise.then((data)=>{
                        if(!data){
                          res.status(500).json({
                            success:false,
                            message:"Something went wrong! Try again later!"
                          });
                        }else{
                          info.success=true;
                          info.message="School created!";
                          res.status(200).json(info);
                        }
                      }).catch((err)=>{
                        console.log(err);
                        res.status(500).json({
                          success:false,
                          message:"Something went wrong! Try again later!"
                        });
                      });
                    
                }
              }).catch((err)=>{
                console.log(err);
                if (err.code == 11000) {
                  res.status(403).json({
                    success: false,
                    message: 'Dublicate key error!'
                  });
                } else {
                  res.status(500).json({
                    success:false,
                    message:"Something went wrong! Try again later! 1"
                  });
                  }
              });
            }
          }).catch((err) => {
            console.log(err);
            res.status(500).json({
              success: false,
              message: "Something went wrong! Try again later! 2"
            });
          });
    }).catch((err)=>{
        console.log(err);
        res.status(500).json({
            success: false,
            message: "Something went wrong! Try again later! 3"
          });
    });
});
module.exports = router;