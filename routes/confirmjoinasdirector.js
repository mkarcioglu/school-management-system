const express = require('express');
const router = express.Router();

const bcrypt = require('bcryptjs');

// Models
const SchoolObject = require('../models/School');
const UserObject = require('../models/User');
const JoinRequestObject = require('../models/JoinRequest');


//Confirm
router.post('/', (req, res, next) => {
  const passcode = req.body.passcode;
  const user_id = req.user_id;
  const promise = JoinRequestObject.findOne({
    requester_id: user_id
  });
  promise.then((data) => {
    console.log(data);
    const school_id = data.school_id;
    const schoolEmail = data.schoolEmail;
    bcrypt.compare(passcode, data.passcode).then((result) => {
      console.log(result);

      if (!result) {

        res.status(404).json({
          success: false,
          message: "Authentication failed. Wrong passcode!"
        });
      } else {

        const promise = UserObject.findByIdAndUpdate(
          user_id, {
            "$addToSet": {
              joinedSchool: {
                authorization: 13,
                school_id: school_id
              }
            }
          }, {
            new: true
          });
        promise.then((data) => {
          console.log("User data: " + data);

          const promise = SchoolObject.findByIdAndUpdate(school_id, {
            requester_id: user_id,
            schoolEmail: schoolEmail
          }, {
            new: true
          });
          promise.then((data) => {

            console.log("School data: " + data);
            if (!data) {
              res.status(500).json({
                success: false,
                message: "Something went wrong! Try again later!"
              });
            } else {

              //## I will an send email function for welcome.
              console.log(data);
              res.status(200).json({
                success: true,
                message: "Welcome!"
              });
            }
          }).catch((err) => {
            console.log(err);
            if (err.code == 11000) {
              res.status(403).json({
                success: false,
                message: 'Dublicate key error!'
              });
            } else {
              res.status(500).json({
                success: false,
                message: "Something went wrong! Try again later! 1"
              });
            }
          });
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Something went wrong! Try again later!"
          });
        });
      } // Else Sonu
    }).catch((err) => {
      console.log(err);
      res.status(500).json({
        success: false,
        message: "Something went wrong! Try again later! 2"
      });
    });
  }).catch((err) => {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong! Try again later! 3"
    });
  });
});
module.exports = router;