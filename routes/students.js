const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const School = require('../models/School');
const ClassObject = require('../models/Class');
const StudentObject = require('../models/Student');
const UserObject = require('../models/User');

// Send Error Response Give
const sendError = (res, statusCode, message) => {
    res.status(statusCode).json({
        success: false,
        message: message
    });
};

//isAuthorized
const isAuthorized = (user_id, school_id, authorizationLevel) => {
    return new Promise((resolve, reject) => {
        const promise = UserObject.findById({
            _id: user_id
        });
        promise.then((data) => {
            console.log(data);
            var auth = false;
            const joinedSchool = data.joinedSchool;
            for (let i = 0; i < joinedSchool.length; i++) {
                const item = joinedSchool[i];
                if (school_id == item.school_id) {
                    if (Number(item.authorization) >= authorizationLevel) {
                        auth = true;
                    }
                }
            }
            resolve(auth);
        }).catch((err) => {
            reject(err);
        });
    });
};

const searchStudent = (res, school_id, keyword) => {
    const promise = StudentObject.find({
        $and: [{
            school_id: school_id
        }, {
            $or: [{
                studentName: {
                    $regex: keyword,
                    $options: "i"
                }
            }, {
                studentSurname: {
                    $regex: keyword,
                    $options: "i"
                }
            }]
        }]
    }, '_id studentNumber studentName studentSurname class_id school_id parent_id scores');
    promise.then((data) => {
        if (!data || data.toString() == "") {
            sendError(res, 404, "There is no student with this name or surname!");
        } else {
            res.status(200).json(data);
        }

    }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
    });
};

// Create ##
router.post('/', (req, res, next) => {
    const {
        user_id,
        studentName,
        studentSurname,
        studentNumber,
        school_id,
        class_id
    } = req.body;
    const promise = isAuthorized(user_id, school_id, 13);
    promise.then((auth) => {
        if (!auth) {
            sendError(res, 401, "You are not authorized!");
        } else {
            const promise = StudentObject.find({
                $and: [{
                    school_id: school_id
                }, {
                    studentNumber: studentNumber
                }]
            });
            promise.then((data) => {
                console.log("Sorgu Datası:" + data + typeof (data));
                // Now we are checking data is empty.
                if (data.toString() == "") {
                    const newStudent = new StudentObject({
                        creator_id: user_id,
                        studentName: studentName,
                        studentSurname: studentSurname,
                        studentNumber: studentNumber,
                        school_id: school_id,
                        class_id: class_id
                    });
                    const promise = newStudent.save();
                    promise.then((data) => {
                        if (!data) {
                            sendError(res, 500, "Something went wrong! Try again later!");
                        } else {
                            res.status(200).json({
                                success: true,
                                message: "Student created!"
                            });
                        }
                    }).catch((err) => {
                        console.log(err);
                        sendError(res, 500, "Something went wrong! Try again later!");
                    });


                } else {
                    sendError(res, 406, "Duplicate error!");
                }

            }).catch((err) => {
                console.log(err);
                sendError(res, 500, "Something went wrong! Try again later!");
            });


        }
    }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
    });
});

// Read-Search-Student
router.get('/singlestudent', (req, res, next) => {
    let {
        user_id,
        school_id,
        studentNumber,
        studentName,
        studentSurname
    } = req.query;

    if (req.query.studentNumber == undefined ||
        req.query.studentName == undefined ||
        req.query.studentSurname == undefined) {
        sendError(res, 406, "You have sent missing or incorrect information!");
    } else {
        let letters = {
            "i": "İ",
            "ş": "Ş",
            "ğ": "Ğ",
            "ü": "Ü",
            "ö": "Ö",
            "ç": "Ç",
            "ı": "I"
        };
        /* studentName = studentName.replace(/(([iışğüçö]))/g, function (letter) {
            return letters[letter];
        });
        studentName = studentName.toUpperCase();
        studentSurname = studentSurname.replace(/(([iışğüçö]))/g, function (letter) {
            return letters[letter];
        });
        studentSurname = studentSurname.toUpperCase(); */
        const promise = isAuthorized(user_id, school_id, 12);
        promise.then((auth) => {
            if (!auth) {
                sendError(res, 401, "You are not authorized!");
            } else {
                if (req.query.studentName == "" &&
                    req.query.studentSurname == "" &&
                    req.query.studentNumber == "") {
                    sendError(res, 406, "You have to sent something!");
                } else {
                    if (studentNumber != "") {
                        const promise = StudentObject.find({
                            $and: [{
                                school_id: school_id
                            }, {
                                studentNumber: studentNumber
                            }]
                        }, '_id studentNumber studentName studentSurname class_id school_id parent_id scores');
                        promise.then((data)=>{
                            if(!data || data.toString() == ""){
                                 sendError(res, 404, "There is no student with this number!");
                            } else {
                                res.status(200).json(data);
                            }
                            
                        }).catch((err)=>{
                            console.log(err);
                            sendError(res, 500, "Something went wrong! Try again later!");
                        });

                    } else {
                        if (studentName.length > 2) {
                            searchStudent(res, school_id, studentName);

                        } else if (studentSurname.length > 2) {
                            searchStudent(res, school_id, studentSurname);

                        } else {
                            sendError(res, 500, "You have to send at least 3 characters.");
                        }
                    }
                }

            }
        }).catch((err) => {
            console.log(err);
            sendError(res, 500, "Something went wrong! Try again later!");
        });

    }
});

// Read-Class
router.get('/studentsintheclassroom', (req, res, next) => {
    const {
        user_id,
        school_id,
        class_id
    } = req.query;
    const promise = isAuthorized(user_id, school_id, 13);
    promise.then((auth) => {
        if (!auth) {
            sendError(res, 401, "You are not authorized!");
        } else {
            const promise = StudentObject.aggregate([{
                    $match: {
                        class_id: mongoose.Types.ObjectId(class_id)
                    }
                },
                {
                    $sort: {
                        studentName: 1
                    }
                }, {
                    $project: {
                        createdAt: false,
                        __v: false,
                        information: false,
                        scores:false
                    }
                }
            ]);
            promise.then((data) => {
                if (!data) {
                    sendError(res, 500, "Something went wrong! Try again later!");
                } else {
                    res.status(200).json(data);
                }
            }).catch((err) => {
                console.log(err);
                sendError(res, 500, "Something went wrong! Try again later!");
            });
        }
    }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
    });
});

// Read-School
router.get('/studentsintheschool', (req, res, next) => {
    const {
        user_id,
        school_id
    } = req.query;
    const promise = isAuthorized(user_id, school_id, 13);
    promise.then((auth) => {
        if (!auth) {
            sendError(res, 401, "You are not authorized!");
        } else {
            const promise = StudentObject.aggregate([{
                    $match: {
                        school_id: mongoose.Types.ObjectId(school_id)
                    }
                },
                {
                    $sort: {
                        studentName: 1
                    }
                }, {
                    $project: {
                        createdAt: false,
                        __v: false,
                        information: false,
                        scores:false
                    }
                }
            ]);
            promise.then((data) => {
                if (!data) {
                    sendError(res, 500, "Something went wrong! Try again later!");
                } else {
                    res.status(200).json(data);
                }
            }).catch((err) => {
                console.log(err);
                sendError(res, 500, "Something went wrong! Try again later!");
            });
        }
    }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
    });
});

// Update ##
router.put('/', (req, res, next) => {
    const {
        student_id,
        user_id,
        studentName,
        studentSurname,
        studentNumber,
        school_id,
        class_id
    } = req.body;
    const promise = isAuthorized(user_id, school_id, 13);
    promise.then((auth) => {
        if (!auth) {
            sendError(res, 401, "You are not authorized!");
        } else {
            const promise= StudentObject.findById(student_id);
            promise.then((data)=>{
                if(!data){
                    sendError(res, 404, "There is no student with this id!");
                } else {
                    if(data.studentNumber==studentNumber){
                        const promise = StudentObject.findByIdAndUpdate(student_id, {
                            studentName: studentName,
                            studentSurname: studentSurname,
                            studentNumber: studentNumber,
                            class_id : class_id
                        },{upsert:false});
                        promise.then((data) => {
                            if (!data) {
                                sendError(res, 404, "There is no student with this id!");
                            } else {
                                res.status(200).json({
                                    success: true,
                                    message: "Student updated!"
                                });
                            }
                        }).catch((err) => {
                            console.log(err);
                            sendError(res, 500, "Something went wrong! Try again later!");
                        });
                    } else {
                        const promise = StudentObject.countDocuments({
                            $and: [{
                                school_id: school_id
                            }, {
                                studentNumber: studentNumber
                            }]
                        });
            
                        promise.then((count)=>{
                            if (count!=0){
                                sendError(res, 406, "There is another student on this number!");   
                            } else {
                                const promise = StudentObject.findByIdAndUpdate(student_id, {
                                    studentName: studentName,
                                    studentSurname: studentSurname,
                                    studentNumber: studentNumber,
                                    class_id : class_id
                                },{upsert:false});
                                promise.then((data) => {
                                    if (!data) {
                                        sendError(res, 404, "There is no student with this id!");
                                    } else {
                                        res.status(200).json({
                                            success: true,
                                            message: "Student updated!"
                                        });
                                    }
                                }).catch((err) => {
                                    console.log(err);
                                    sendError(res, 500, "Something went wrong! Try again later!");
                                });
                            }
                        }).catch((err)=>{
                            console.log(err);
                            sendError(res, 500, "Something went wrong! Try again later!");
                        });
                    }
                }
            }).catch((err)=>{
                console.log(err);
                sendError(res, 500, "Something went wrong! Try again later!");
            });
        }
    }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
    });
});

// Delete
router.delete('/', (req, res, next)=>{
    const {
        student_id,
        user_id,
        school_id
    } = req.body;
    const promise = isAuthorized(user_id, school_id, 13);

    promise.then((auth) => {
        if (!auth) {
            sendError(res, 401, "You are not authorized!");
        } else {
            const promise= StudentObject.findByIdAndDelete(student_id);
            promise.then((data)=>{
                if(!data){
                    sendError(res, 404, "There is no student with this id!");
                } else {
                    res.status(200).json({
                        success: true,
                        message: "Student deleted!"
                    });
                }
            }).catch((err)=>{
                console.log(err);
                sendError(res, 500, "Something went wrong! Try again later!");
            });
        }
    }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
    });
});

// Special Condition Add
router.post('/addinfo', (req, res, next)=>{
    const {
        user_id,
        school_id,
        student_id,
        information
    } = req.body;
    const promise = isAuthorized(user_id, school_id, 13);

    promise.then((data) => {
        if (!data) {
            sendError(res, 401, "You are not authorized!");
        } else {
            const promise=StudentObject.findByIdAndUpdate(student_id, {information},{new:true});
            promise.then((data)=>{
                if(!data){
                    sendError(res, 404, "There is no student with this id!");
                } else {
                    res.status(200).json({
                        success:true,
                        message: "Information added!"
                    });
                }
            }).catch((err)=>{
                console.log(err);
                sendError(res, 500, "Something went wrong! Try again later!");
            });
        }
    }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
    });

});

module.exports = router;