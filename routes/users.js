const express = require('express');
const router = express.Router();

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

//Models
const MyUserObject = require('../models/User');


// Update Info
router.put('/updatemyinfo', (req, res, next) => {

  const {
    name,
    surname,
    email,
    user_id,
    password
  } = req.body;
  if (user_id == req.user_id) {
    console.log("ID match!");
    const promise = MyUserObject.findById({
      _id: user_id
    });
    promise.then((user) => {
      if (!user) {
        res.status(404).json({
          success: false,
          message: "That's weird! User not found!"
        });
      } else {
        bcrypt.compare(password, user.password).then((result) => {
          if (!result) {
            res.status(404).json({
              success: false,
              message: "Authentication failed. Wrong password!"
            });
          } else {
            const promise = MyUserObject.findByIdAndUpdate({
              _id: user._id
            }, {
              name: name,
              surname: surname,
              email: email,
            });
            promise.then((data) => {
 
              if (user.email != email && user.mail != null) {
                let transporter = nodemailer.createTransport({
                  host: process.env.NODEMAILER_HOST,
                  auth: {
                    user: process.env.NODEMAILER_USER,
                    pass: process.env.NODEMAILER_PASS
                  }
                });

                let mailOptions = {
                  from: process.env.NODEMAILER_USER,
                  to: user.email,
                  subject: ' Ulak Security Info',
                  text: "Dear " + user.name + " " + user.surname + "\n\nYour password has changed. We want to let you know."
                };

                transporter.sendMail(mailOptions, (error, info) => {
                  if (error) {
                    return console.log(error);
                  } else {
                    console.log('Message sent: %s', info.messageId);
                    // Preview only available when sending through an Ethereal account
                    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                  }
                });
              } else if (user.email != email && user.email == null && email != null) {
                let transporter = nodemailer.createTransport({
                  host: process.env.NODEMAILER_HOST,
                  auth: {
                    user: process.env.NODEMAILER_USER,
                    pass: process.env.NODEMAILER_PASS
                  }
                });

                let mailOptions = {
                  from: process.env.NODEMAILER_USER,
                  to: email,
                  subject: 'Welcome to Ulak School Systems!',
                  text: "Dear " + user.name + " " + user.surname + "\n\nWelcome to Ulak School Systems! You receive this email because you have updated your email adress. If you are not " + user.name + " " + user.surname + " ignore this email plase. Thank you!"
                };

                transporter.sendMail(mailOptions, (error, info) => {
                  if (error) {
                    return console.log(error);
                  } else {
                    console.log('Message sent: %s', info.messageId);
                    // Preview only available when sending through an Ethereal account
                    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                  }
                });
              }

              res.status(200).json({
                success: true,
                message: "Success!"
              });
            }).catch((err) => {
              console.log(err);
              const response = {
                success: false,
                errorName: err.name,
                errorCode: err.code,
                errorCodeName: err.codeName,
                message: err.errmsg
              };
              if (err.code == 11000) {
                res.status(403).json({
                  success: false,
                  message: 'Dublicate key error!'
                });
              } else {
                res.status(403).json(response);
              }
            });


          }
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Internal server error!"
          });
        });
      }

    }).catch((err) => {
      console.log(err);
      res.status(500).json({
        success: false,
        message: "Something went wrong. Try again later."
      });
    });
  } else {
    console.log("Different ID values!");
    res.status(500).json({
      success: false,
      message: "There is something wrong about your ID! Login again!"
    });
  }
});

// Change Password
router.put('/changepassword', (req, res, next) => {

  const {
    user_id,
    password,
    newPassword    
  } = req.body;
  if (user_id == req.user_id) {
    console.log("ID match!");
    const promise = MyUserObject.findById({
      _id: user_id
    });
    promise.then((user) => {
      if (!user) {
        res.status(404).json({
          success: false,
          message: "That's weird! User not found!"
        });
      } else {
        bcrypt.compare(password, user.password).then((result) => {

          // burada result değeri true ya da false dönüyor.
          if (!result) {
            // Şifre hatalıysa ilgili mesajı gönderelim. 
            res.status(404).json({
              success: false,
              message: "Authentication failed. Wrong password!"
            });
          } else {
            bcrypt.hash(newPassword, 10).then((hash) => {
              const promise = MyUserObject.findByIdAndUpdate({
                _id: user._id
              }, {
                password: hash,
              });
              promise.then((data) => {
                const receiver = {
                  name: user.name,
                  surname: user.surname
                };
                let transporter = nodemailer.createTransport({
                  host: process.env.NODEMAILER_HOST,
                  auth: {
                    user: process.env.NODEMAILER_USER,
                    pass: process.env.NODEMAILER_PASS
                  }
                });

                let mailOptions = {
                  from: process.env.NODEMAILER_USER,
                  to: user.email,
                  subject: ' Ulak School System Security Information',
                  text: "Dear " + receiver.name + " " + receiver.surname + "\n\nYour password has changed successfully!"
                };

                transporter.sendMail(mailOptions, (error, info) => {
                  if (error) {
                    return console.log(error);
                  } else {
                    console.log('Message sent: %s', info.messageId);
                    // Preview only available when sending through an Ethereal account
                    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                  }
                });

                res.status(200).json({
                  success: true,
                  message: "Success!"
                });
              }).catch((err) => {
                console.log(err);
                const response = {
                  success: false,
                  errorName: err.name,
                  errorCode: err.code,
                  errorCodeName: err.codeName,
                  message: err.errmsg
                };
                res.status(403).json(response);
              });
            }).catch((err) => {
              console.log(err);
              res.status(500).json({
                success: false,
                message: "Internal server error!"
              });
            });
          }
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Internal server error!"
          });
        });
      }

    }).catch((err) => {
      console.log(err);
      res.status(500).json({
        success: false,
        message: "Something went wrong. Try again later."
      });
    });
  } else {
    console.log("Different ID values!");
    res.status(500).json({
      success: false,
      message: "There is something wrong about your ID! Login again!"
    });
  }
});

// Add And Update Phone
router.put('/updatemyphone', (req, res, next) => {

  const {
    user_id,
    password,
    phone
  } = req.body;
  if (user_id == req.user_id) {
    console.log("ID match!");
    const promise = MyUserObject.findById({
      _id: user_id
    });
    promise.then((user) => {
      if (!user) {
        res.status(404).json({
          success: false,
          message: "That's weird! User not found!"
        });
      } else {
        bcrypt.compare(password, user.password).then((result) => {
          if (!result) {

            res.status(404).json({
              success: false,
              message: "Authentication failed. Wrong password!"
            });
          } else {
            const promise = MyUserObject.findByIdAndUpdate({
              _id: user._id
            }, {
              phone: phone,
              isVerified: true
            });
            promise.then((data) => {
              const receiver = {
                name: user.name,
                surname: user.surname
              };
              if (user.email) {

                let transporter = nodemailer.createTransport({
                  host: process.env.NODEMAILER_HOST,
                  auth: {
                    user: process.env.NODEMAILER_USER,
                    pass: process.env.NODEMAILER_PASS
                  }
                });

                let mailOptions = {
                  from: process.env.NODEMAILER_USER,
                  to: user.email,
                  subject: 'Ulak School System Information',
                  text: "Dear " + receiver.name + " " + receiver.surname + "\n\nYour phone info has changed. Thank you."
                };

                transporter.sendMail(mailOptions, (error, info) => {
                  if (error) {
                    return console.log(error);
                  } else {
                    console.log('Message sent: %s', info.messageId);
                    // Preview only available when sending through an Ethereal account
                    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                  }
                });

                //**************************************************** */
              }

              res.status(200).json({
                success: true,
                message: "Success!"
              });
            }).catch((err) => {
              console.log(err);
              const response = {
                success: false,
                errorName: err.name,
                errorCode: err.code,
                errorCodeName: err.codeName,
                message: err.errmsg
              };
              res.status(403).json(response);
            });
          }
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Internal server error!"
          });
        });
      }

    }).catch((err) => {
      console.log(err);
      res.status(500).json({
        success: false,
        message: "Something went wrong. Try again later."
      });
    });
  } else {
    console.log("Different ID values!");
    res.status(500).json({
      success: false,
      message: "There is something wrong about your ID! Login again!"
    });
  }
});

// Delete user
router.delete('/', (req, res, next) => {
  const {
    user_id,
    password
  } = req.body;
  if (user_id != req.user_id) {
    res.status(500).json({
      success: false,
      message: "There is something wrong about your ID! Login again!"
    });
  } else {
    const _id = user_id;
    const promise = MyUserObject.findById({
      _id
    });
    promise.then((user) => {
      if (!user) {

        res.status(404).json({
          success: false,
          message: "Authentication failed. User not found!"
        });
      }else{
        bcrypt.compare(password, user.password).then((result) => {
          if (!result) {

            res.status(404).json({
              success: false,
              message: "Authentication failed. Wrong password!"
            });
          } else {
            // Password correct. So lets' delete user. 
            const promise=MyUserObject.findByIdAndDelete({_id});
            promise.then((data)=>{
              res.status(200).json({
                success: true,
                message: "User deleted!"
              });
            }).catch((err)=>{
              console.log(err);
              res.status(500).json({
                success: false,
                message: "Internal server error!"
              });
            });
          }
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Internal server error!"
          });
        });
      }

    }).catch((err) => {
      console.log(err);
      res.status(500).json({
        success: false,
        message: "Something went wrong! Try again later!"
      });

    });
  }


});

// Get user info ##
router.get('/', (req, res, next)=>{

});

module.exports = router;