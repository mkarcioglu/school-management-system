const express = require('express');
const router = express.Router();

const bcrypt = require('bcryptjs');

const SchoolObject = require('../models/School');
const SchoolDeletionTempObject = require('../models/SchoolDeletionTemp');

router.post('/verifypasscode', (req, res, next)=>{
    const passcode=req.body.passcode;
    const user_id=req.user_id;
    if (req.body.user_id!=req.user_id){
        res.status(403).json({
            success: false,
            message: "There is something wrong with your id!"
        });
    }else{
        const promise=SchoolDeletionTempObject.findOne({
            creator_id:user_id
        });
        promise.then((data)=>{
            console.log(data);
            
            bcrypt.compare(passcode, data.passcode).then((result) => {
                console.log(result);
      
                if (!result) {

                  res.status(404).json({
                    success: false,
                    message: "Authentication failed. Wrong passcode!"
                  });
                } else {

                  const _id=data.school_id;
                  const promise=SchoolObject.findByIdAndDelete({
                      _id
                  });
                  promise.then((data)=>{
                    res.status(200).json({
                        success: true,
                        message: "School deleted!"
                      });
                  }).catch((err)=>{
                    console.log(err);
                    res.status(200).json({
                        success: false,
                        message: "Something went wrong! Try again later!"
                      });
                  });
                 
                }
              }).catch((err) => {
                console.log(err);
                res.status(500).json({
                  success: false,
                  message: "Something went wrong! Try again later!"
                });
              });
        }).catch((err)=>{
            console.log(err);
            res.status(500).json({
                success: false,
                message: "Something went wrong! Try again later!"
              });
        });
    }
    
});
module.exports = router;