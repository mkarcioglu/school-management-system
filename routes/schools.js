const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

const School = require('../models/School');
const SchoolCreationTemp = require('../models/SchoolCreationTemp');
const SchoolDeletionTemp = require('../models/SchoolDeletionTemp');
const UserObject = require('../models/User');


// Create
router.post('/', (req, res, next) => {
  const {
    user_id,
    school_name,
    state,
    city,
    school_phone,
    school_email,
    fax,
    address,
    category
  } = req.body;
  if (!school_email) {
    res.status(406).json({
      success: false,
      message: "A valid school email address required!"
    });
  } else {
    const sp = school_email.split('@');
    if (sp[1] != 'meb.k12.tr') {
      res.status(406).json({
        success: false,
        message: "A valid school email address required!"
      });
    } else {
      console.log(sp[0] + sp[1]);
      const num = Math.floor(Math.random() * (999999 - 100000)) + 100000;
      const passcode = num.toString();

      bcrypt.hash(passcode, 10).then((hash) => {
        const promise = SchoolCreationTemp.updateOne({
          schoolEmail:school_email
        }, {
          creator_id: user_id,
          schoolName: school_name,
          state,
          city,
          schoolPhone: school_phone,
          schoolEmail: school_email,
          fax,
          address,
          category,
          passcode: hash
        }, {
          upsert:true
        });

        promise.then((data) => {
          //res.json(data);
          console.log("Db kayıt başarılı!");
          console.log(data);
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Something went wrong. Try again later."
          });
        });
      });
      const _id = mongoose.Types.ObjectId(user_id);
      const promise = UserObject.findById({
        _id
      });
      promise.then((user) => {
        const receiver = {
          name: user.name,
          surname: user.surname
        };
        let transporter = nodemailer.createTransport({
          host: process.env.NODEMAILER_HOST,
          auth: {
            user: process.env.NODEMAILER_USER,
            pass: process.env.NODEMAILER_PASS
          }
        });

        let mailOptions = {
          from: process.env.NODEMAILER_USER,
          to: school_email,
          subject: passcode + ' This is your passcode.',
          text: "Sayın " + receiver.name + " " + receiver.surname + "\n\nThis is your passcode. It will expire in 15 minutes. " + passcode
        };

        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            res.status(500).json({
              success: false,
              message: "Something went wrong. Try again later."
            });
            return console.log(error);
          } else {
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            const payload = {
              user_id
            };
            const temptoken = jwt.sign(payload, process.env.TEMPORARILYTOKENKEY, {
              expiresIn: 900
            });

            res.status(200).json({
              success: true,
              message: "If your address is correct, you will receive an email!",
              temptoken: temptoken,
              passcode: passcode
              // IMPORTANT: THIS IS OTP TEST LINE. YOU HAVE TO COMMENT OUT WHEN YOU FINISH YOUR TEST
            });

          }

        });


      }).catch((err) => {
        console.log(err);
        res.status(500).json({
          success: false,
          message: "Something went wrong! Try again later!"
        });
      });
    }
  }
});

// Read-Search
router.get('/', (req, res, next) => {
  // state, city and category may be empty but not keyword
  let state = "";
  let city = "";
  let category = "";
  if (req.query.state !== undefined) state = req.query.state;
  if (req.query.city !== undefined) city = req.query.city;
  if (req.query.category !== undefined) category = req.query.category;
  let letters = { "i": "İ", "ş": "Ş", "ğ": "Ğ", "ü": "Ü", "ö": "Ö", "ç": "Ç", "ı": "I" };
  city = city.replace(/(([iışğüçö]))/g, function(letter){ return letters[letter]; });
  city=city.toUpperCase();
  state = state.replace(/(([iışğüçö]))/g, function(letter){ return letters[letter]; });
  state=state.toUpperCase();
  
  if (req.query.keyword==undefined){
    res.status(406).json({
      success: false,
      message: "Keyword required to search!"
    });
  }else if ((req.query.keyword).length<3){
    res.status(406).json({
      success: false,
      message: "Keyword can not be less than 3 characters!"
    });
  } else{
    const promise = School.find({
      $and: [{
        schoolName: {
          $regex: req.query.keyword,
          $options: "i"
        }
      }, {
        state: {
          $regex: state,
          $options: "i"
        }
      }, {
        city: {
          $regex: city,
          $options: "i"
        }
      }, {
        category: {
          $regex: category,
          $options: "i"
        }
      }]
    },
    'schoolName state city');
  promise.then((data) => {
    res.json(data);
  }).catch((err) => {
    const response = {
      errorName: err.name,
      errorCode: err.code,
      errorCodeName: err.codeName,
      errorMessage: err.errmsg

    };
    response.success = false;
    response.message = "Forbidden!";
    if (err.code == 2) {
      res.status(401).json({
        success: false,
        message: 'Keyword can not be empty!'
      });
    } else {
      res.status(403).json(response);
    }
  });
  }  
});

// Update ##
router.put('/', (req, res, next) => {
  const {
    school_id,
    user_id,
    school_name,
    state,
    city,
    school_phone,
    school_email,
    fax,
    address,
    category
  } = req.body;
  
  if (!school_email || req.body.school_email == undefined) {
    res.status(406).json({
      success: false,
      message: "A valid school email address required!"
    });
  } else {
    const sp = school_email.split('@');
    if (sp[1] != 'meb.k12.tr') {
      res.status(406).json({
        success: false,
        message: "A valid school email address required!"
      });
    } else {
        const _id = school_id;
        const promise=School.findById({_id});
        promise.then((data)=>{
          if(data.creator_id != user_id){
            res.status(403).json({
              success:false,
              message:"Forbidden! You are not creator of this school!"
            });
          }
          else{
            if (req.user_id != user_id) {
              res.status(406).json({
                success: false,
                message: "There is something wrong about your ID!"
              });
            } else {
              
              var needEmail = null;
              var oldEmail = null;
              if (!data.schoolEmail && data.schoolEmail != school_email) {
                needEmail = true;
                oldEmail = data.schoolEmail;
              } else {
                needEmail = false;
              }
      
              const promise = School.findByIdAndUpdate(_id, {
                creator_id: user_id,
                schoolName: school_name,
                state,
                city,
                address,
                schoolPhone: school_phone,
                schoolEmail: school_email,
                fax,
                category
              }, {
                new: true
              });
              promise.then((data) => {
                res.status(200).json({
                  success: true,
                  message: 'School updated!'
                });
                if (needEmail) {
                  const _id = mongoose.Types.ObjectId(user_id);
                  const promise = UserObject.findById({
                    _id
                  });
                  promise.then((user) => {
                    const receiver = {
                      name: user.name,
                      surname: user.surname
                    };
                    let transporter = nodemailer.createTransport({
                      host: process.env.NODEMAILER_HOST,
                      auth: {
                        user: process.env.NODEMAILER_USER,
                        pass: process.env.NODEMAILER_PASS
                      }
                    });
      
                    let mailOptions = {
                      from: process.env.NODEMAILER_USER,
                      to: oldEmail,
                      subject: ' Security information!',
                      text: "Dear " + receiver.name + " " + receiver.surname + "\n\nSome information has changed in Ulak School Systems. We want to let you know."
                    };
      
                    transporter.sendMail(mailOptions, (error, info) => {
                      if (error) {
                        return console.log(error);
                      } else {
                        console.log('Message sent: %s', info.messageId);
                        // Preview only available when sending through an Ethereal account
                        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                      }
      
                    });
      
      
                  }).catch((err) => {
                    console.log(err);
                    res.status(500).json({
                      success: false,
                      message: "Something went wrong! Try again later!"
                    });
                  });
                }
              }).catch((err) => {
                console.log(err);
                res.status(500).json({
                  success:false,
                  message: "Something went wrong! Try again later!"
                });
              });
            }
          }
        }).catch((err)=>{
          console.log(err);
          res.status(500).json({
            success:false,
            message: "Something went wrong! Try again later!"
          });
        });
    }
  }
});

// Delete
router.delete('/', (req, res, next) => {

  const {
    user_id,
    school_id
  } = req.body;
  const _id = school_id;
  const promise = School.findById({
    _id
  });
  promise.then((data) => {

    if (!data.creator_id) {
      res.status(406).json({
        success: false,
        message: "You are not creator of this school!"
      });
    } else {
      const creator_id = data.creator_id;
      if (user_id != creator_id) {
        res.status(403).json({
          success: false,
          message: "You are not creator of this school!"
        });
      } else {
        if (!data.schoolEmail) {
          res.status(403).json({
            success: false,
            message: "That's weird. Your school has no an email adress yet!"
          });
        } else {

          const num = Math.floor(Math.random() * (999999 - 100000)) + 100000;
          const passcode = num.toString();

          bcrypt.hash(passcode, 10).then((hash) => {
            const schooldeletiontemp = new SchoolDeletionTemp({
              creator_id: user_id,
              school_id,
              passcode: hash
            });

            const promise = schooldeletiontemp.save();
            promise.then((data) => {
              //res.json(data);
              console.log("Db save successful!");
              console.log(data);
            }).catch((err) => {
              console.log(err);
              res.status(500).json({
                success: false,
                message: "Something went wrong. Try again later."
              });
            });

          });

          // Şimdi Mail Göndereceğiz************************************/
          const _id = mongoose.Types.ObjectId(user_id);
          const promise = UserObject.findById({
            _id
          });
          promise.then((user) => {
            const receiver = {
              name: user.name,
              surname: user.surname
            };
            let transporter = nodemailer.createTransport({
              host: process.env.NODEMAILER_HOST,
              auth: {
                user: process.env.NODEMAILER_USER,
                pass: process.env.NODEMAILER_PASS
              }
            });

            console.log(data.schoolEmail);

            let mailOptions = {
              from: process.env.NODEMAILER_USER,
              to: data.schoolEmail,
              subject: passcode + 'Ulak Security Code',
              text: "Dear " + receiver.name + " " + receiver.surname + "\n\nHere is security code to delete your school from Ulak System: " + passcode
            };

            transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                res.status(500).json({
                  success: false,
                  message: "Something went wrong. Try again later."
                });
                return console.log(error);
              } else {
                console.log('Message sent: %s', info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                const payload = {
                  user_id
                };
                const temptoken = jwt.sign(payload, process.env.TEMPORARILYTOKENKEY, {
                  expiresIn: 900
                });

                res.status(200).json({
                  success: true,
                  message: "If your address is correct, you will receive an email!",
                  temptoken: temptoken,
                  passcode: passcode
                  // IMPORTANT: THIS IS OTP TEST LINE. YOU HAVE TO COMMENT OUT WHEN YOU FINISH YOUR TEST
                });

              }

            });

          }).catch((err) => {
            console.log(err);
            res.status(500).json({
              success: false,
              message: "Something went wrong! Try again later!"
            });
          });
        }
      }
    }

  }).catch((err) => {
    console.log(err);
    res.status(500).json({
      success: false,
      message: "Something went wrong! Try again later!"
    });
  });

});


module.exports = router;