const express = require('express');
const router = express.Router();

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');


//Models
const MyUserObject = require('../models/User');
const MyLostObject = require('../models/Lost');


// Verify Passcode 
router.post('/verifypasscode', (req, res, next) => {
  const {
    email,
    passcode
  } = req.body;
  const decoded_email = req.decoded_email;
  console.log("VPass decoded email: " + decoded_email);
  if (email == decoded_email) {
    const promise = MyLostObject.findOne({
      email
    });
    promise.then((data) => {
      console.log("Sorgu datası:" + data);
      console.log("Gelen Passcode:" + passcode);

      if (!data) {
        res.status(403).json({
          success: false,
          message: "Oops! We are sorry! Something went wrong!"
        });
      } else {

        bcrypt.compare(passcode, data.passcode).then((result) => {
          console.log(result);

          if (!result) {
            res.status(404).json({
              success: false,
              message: "Authentication failed. Wrong passcode!"
            });
          } else {
            const payload = {
              email
            };
            const temptoken = jwt.sign(payload, process.env.TEMPORARILYTOKENKEY, {
              expiresIn: 600
            });

            res.status(200).json({
              success: true,
              message: "Passcode true!",
              temptoken: temptoken
            });
          }
        }).catch((err) => {
          console.log(err);
          res.status(500).json({
            success: false,
            message: "Internal server error!"
          });
        });

      }

    }).catch((err) => {
      console.log(err);
      res.status(500).json({
        success: false,
        message: "Something went wrong. Try again later."
      });

    });
  }else{
    res.status(401).json({
      success: false,
      message: "Unauthorized!"
    });
  }



});

// New Password
router.post('/newpassword', (req, res, next) => {

  const {
    email,
    password
  } = req.body;
  const decoded_email = req.decoded_email;
  console.log("newpassword decoded email: " + decoded_email);
  if (email == decoded_email) {
    bcrypt.hash(password, 10).then((hash) => {
      const promise = MyUserObject.findOneAndUpdate(email, {
        password: hash
      }, {
        new: true
      });
      promise.then((user) => {
        const receiver = {
          name: user.name,
          surname: user.surname
        };
        let transporter = nodemailer.createTransport({
          host: process.env.NODEMAILER_HOST,
          auth: {
            user: process.env.NODEMAILER_USER,
            pass: process.env.NODEMAILER_PASS
          }
        });

        let mailOptions = {
          from: process.env.NODEMAILER_USER,
          to: email,
          subject: ' Ulak Şifre Değişikliği',
          text: "Sayın " + receiver.name + " " + receiver.surname + "\n\nUlak şifreniz değişti."
        };

        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return console.log(error);
          } else {
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
          }
        });

        res.status(200).json({
          success: true,
          message: "Success!"
        });
      }).catch((err) => {
        console.log(err);
        res.status(500).json({
          success: false,
          message: "Internal server error!"
        });
      });
    });
  } else {
    res.status(401).json({
      success: false,
      message: "Unauthorized!"
    });
  }



});

module.exports = router;