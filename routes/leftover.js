// Read
router.get('/search/:keyword?', (req, res, next) => {

    MyUserObject.find({
        $or: [{
          name: {
            $regex: req.params.keyword,
            $options: "i"
          }
        }, {
          surname: {
            $regex: req.params.keyword,
            $options: "i"
          }
        }]
      },
      'name surname email', (err, data) => {
        if (err) {
  
          const response = {
            errorName: err.name,
            errorCode: err.code,
            errorCodeName: err.codeName,
            errorMessage: err.errmsg
          };
          if (err.code == 2) {
            res.status(401).json({
              success: false,
              msg: 'Keyword can not be empty!'
            });
          } else res.status(403).json({
            success: false,
            msg: 'Forbidden!'
          });
        } else {
          res.json(data);
        }
      });
  
  });
  
  // GetInfo
  router.post('/getmyinfo', (req, res, next) => {
  
    MyUserObject.findById(req.body.userId, (err, data) => {
      if (err) {
  
        const response = {
          errorName: err.name,
          errorCode: err.code,
          errorCodeName: err.codeName,
          errorMessage: err.errmsg
        };
        if (err.code == 2) {
          res.status(401).json({
            success: false,
            msg: 'Keyword can not be empty!'
          });
        } else res.status(403).json({
          success: false,
          msg: 'Forbidden!'
        });
      } else {
        res.json(data);
      }
    });
  
  });
  // Update
  
  router.put('/update', (req, res, next) => {
  
    MyUserObject.findByIdAndUpdate(req.body.userId, {
      name: req.body.name,
      surname: req.body.surname,
      email: req.body.email,
    }, {
      new: true
    }, (err, data) => {
      if (err) {
  
        const response = {
          errorName: err.name,
          errorCode: err.code,
          errorCodeName: err.codeName,
          errorMessage: err.errmsg
        };
        res.status(403).send(response);
        console.log(err);
      } else if (data == null) {
        res.status(404).json({
          success: false,
          msg: 'Not found'
        });
      } else {
        res.status(200).json({
          success: true,
          msg: 'Update completed!'
        });
      }
    });
  });
  
  // Delete
  
  router.delete('/delete', (req, res, next) => {
  
    MyUserObject.findByIdAndDelete(req.body.userId, (err, data) => {
      if (err) {
        const response = {
          errorName: err.name,
          errorCode: err.code,
          errorCodeName: err.codeName,
          errorMessage: err.errmsg,
          errorMessage2: err.message,
          errorKind: err.kind
        };
        if (err.name == "CastError") res.status(404).json({
          success: false,
          msg: 'MyUserObject not found'
        });
        else res.status(403).send(response);
        console.log(err);
      } else if (data == null) {
        res.status(404).json({
          success: false,
          msg: 'Not found'
        });
      } else {
        res.status(200).json({
          success: true,
          msg: 'Completed!'
        });
      }
    });
  });