const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const SchoolObject = require('../models/School');
const ClassObject = require('../models/Class');
const UserObject = require('../models/User');

// Send Error Response Give
const sendError = (res, statusCode, message) => {
    res.status(statusCode).json({
        success: false,
        message: message
    });
};


//isAuthorized
const isAuthorized = (user_id, school_id, authorizationLevel) => {
    return new Promise((resolve, reject) => {
        const promise = UserObject.findById({
            _id: user_id
        });
        promise.then((data) => {
            console.log(data);
            var auth = false;
            const joinedSchool = data.joinedSchool;
            for (let i = 0; i < joinedSchool.length; i++) {
                const item = joinedSchool[i];
                if (school_id == item.school_id) {
                    if (Number(item.authorization) >= authorizationLevel) {
                        auth = true;
                    }
                }
            }
            resolve(auth);
        }).catch((err) => {
            reject(err);
        });
    });
};

// Create
router.post('/', (req, res, next) => {
    const {
        user_id,
        school_id,
        teacher_id,
        className
    } = req.body;
    const promise = isAuthorized(user_id, school_id, 13);
    
    promise.then((data) => {
        if (!data) {
            sendError(res, 401, "You are not authorized!");
        } else {
            const newClass = new ClassObject({
                creator_id: user_id,
                school_id: school_id,
                tutor_id: teacher_id,
                className: className
            });
            const promise = newClass.save();
            promise.then((data) => {
                if (!data) {
                    sendError(res, 500, "Something went wrong! Try again later!");
                } else {
                    res.status(200).json({
                        success: true,
                        message: "Class created!"
                    });
                }
            }).catch((err) => {
                console.log(err);
                res.status(500).json({
                    success: false,
                    message: "Something went wrong! Try again later! 2"
                });
            });
        }
    }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
    });
});

// Read-List all classes
router.get('/', (req, res, next) => {
    const {
        user_id,
        school_id,
    } = req.query;
    if (req.query.user_id == undefined || req.query.school_id == undefined) {
        sendError(res, 406, "You have sent missing or incorrect information!");
    } else {
        const promise = isAuthorized(user_id, school_id, 13);
        promise.then((auth) => {
            if (!auth) {
                sendError(res, 401, "You are not authorized!");
            } else {
                const promise = ClassObject.aggregate([{
                    $match: {
                        school_id: mongoose.Types.ObjectId(school_id)
                    }
                }, {
                    $project: {
                        _id: 1,
                        creator_id: 1,
                        tutor_id: 1,
                        className: 1
                    }
                }]);
                promise.then((data) => {
                    if (!data) {
                        sendError(res, 500, "Something went wrong! Try again later!");
                    } else {
                        res.status(200).json(data);
                    }
                }).catch((err) => {
                    console.log(err);
                    sendError(res, 500, "Something went wrong! Try again later!");
                });
            }
        }).catch((err) => {
            console.log(err);
            sendError(res, 500, "Something went wrong! Try again later!");
        });
    }

});

// Update ##
router.put('/', (req, res, next) => {
    const {
        class_id,
        user_id,
        school_id,
        teacher_id,
        className
    } = req.body;
    const promise = isAuthorized(user_id, school_id, 13); 
    promise.then((data) => {
        if (!data) {
            sendError(res, 401, "You are not authorized!");
        } else {
            if (req.body.className == undefined) {
                sendError(res, 406, "Classname cannot be empty!");
            } else {
                const promise = ClassObject.findByIdAndUpdate(class_id, {
                    tutor_id: teacher_id,
                    className: className
                });
                promise.then((data) => {
                    if (!data) {
                        sendError(res, 500, "Something went wrong! Try again later!");
                    } else {
                        res.status(200).json({
                            success: true,
                            message: "Class updated!"
                        });
                    }
                }).catch((err) => {
                    console.log(err);
                    res.status(500).json({
                        success: false,
                        message: "Something went wrong! Try again later! 2"
                    });
                });
            }

        }
    }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
    });
});

// Delete
router.delete('/', (req, res, next) => {

    const {
        class_id,
        user_id,
        school_id
    } = req.body;
    const promise = isAuthorized(user_id, school_id, 13);
    promise.then((data) => {
        if (!data) {
            sendError(res, 401, "You are not authorized!");
        } else {
                const promise = ClassObject.findByIdAndDelete(class_id);
                promise.then((data) => {
                    if (!data) {
                        sendError(res, 500, "Something went wrong! Try again later!");
                    } else {
                        res.status(200).json({
                            success: true,
                            message: "Class deleted!"
                        });
                    }
                }).catch((err) => {
                    console.log(err);
                    res.status(500).json({
                        success: false,
                        message: "Something went wrong! Try again later!"
                    });
                });
            }
    }).catch((err) => {
        console.log(err);
        sendError(res, 500, "Something went wrong! Try again later!");
    });

});

module.exports = router;