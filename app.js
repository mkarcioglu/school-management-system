const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const multer = require('multer');


const indexRouter = require('./routes/index');
const lostRouter = require('./routes/lost');
const usersRouter = require('./routes/users');
const schoolsRouter = require('./routes/schools');
const classesRouter = require('./routes/classes');
const confirmCreationRouter = require('./routes/confirmcreation');
const confirmDeletionRouter = require('./routes/confirmdeletion');
const confirmJoinRouter = require('./routes/confirmjoinasdirector');
const studentsRouter = require('./routes/students');
const joinsRouter = require('./routes/joins');
const newsRouter = require('./routes/news');

const app = express();
// db connection
const db =require('./helper/db')();

//Config api key
// const config=require('./config');
// app.set('api_secret_key', config.api_secret_key);
require('dotenv').config();

// Middleware
const verifyBaseToken=require('./middleware/basetoken');
const verifyLostPassToken=require('./middleware/lostpasstoken');
const verifyTempToken=require('./middleware/temptoken');
const isVerified=require('./middleware/isVerified');

// view engine setup
/* app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade'); */

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', indexRouter);
app.use('/api', verifyBaseToken);
app.use('/lost', verifyLostPassToken, lostRouter);
app.use('/confirmcreation', verifyTempToken, confirmCreationRouter);
app.use('/confirmdeletion', verifyTempToken, confirmDeletionRouter);
app.use('/confirmjoinasdirector', verifyTempToken, confirmJoinRouter);
app.use('/api/users', usersRouter);
app.use('/api/students', studentsRouter);
app.use('/api/schools', schoolsRouter);
app.use('/api/classes', classesRouter);
app.use('/api/joins', isVerified, joinsRouter);
app.use('/api/news', newsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.status(err.status).json({message:err.message});
  //res.locals.message = err.message;
  //res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  // res.status(err.status || 500);
  // res.render('error');
});

module.exports = app;
