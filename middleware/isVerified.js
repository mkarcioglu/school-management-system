const express = require('express');
const router = express.Router();
const multer= require('multer');

// Model
const UserObject = require('../models/User');


router.post('/', (req, res, next)=>{
    const user_id=req.body.user_id || req.query.user_id;
    const promise= UserObject.findById(
        user_id
    );
    promise.then((user)=>{
        console.log(user);
        if(user.isVerified){
            req.phone=user.phone;
            req.name=user.name;
            req.surname=user.surname;
            next();
        }else{
          res.status(406).json({
            success: false,
            message: "You are not verified!"
        });
        }
    }).catch((err)=>{
        if (user_id==undefined){
            res.status(406).json({
                success:false,
                message: "User ID missing!"
            });
        }else{
            console.log(err);
            res.status(500).json({
                success:false,
                message: "Internal server error!"
            });
        }
        
    });
    
});

module.exports=router;