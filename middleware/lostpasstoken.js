const jwt=require('jsonwebtoken');

module.exports=(req, res, next)=>{
    const temptoken = req.headers['x-access-temptoken'] || req.body.temptoken || req.query.temptoken;
    if (temptoken){
        jwt.verify(temptoken, process.env.TEMPORARILYTOKENKEY,(err, decoded)=>{
            if(err){
                res.status(403).json({
                    status: false,
                    message: "Failed to authenticate token!"
                });
            }else{                
                req.decoded_email=decoded.email;
                console.log("Decoded verisi:");
                console.log(decoded);
                next();
            }

        });
    }else{ 
        res.status(403).json({
            status: false,
            message: "Forbidden!"
        });
    }
};