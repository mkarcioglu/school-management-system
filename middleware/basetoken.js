const jwt = require('jsonwebtoken');
module.exports = (req, res, next) => {
    const token = req.headers['x-access-token'] || req.body.token || req.query.token;
    if (token) { 

        jwt.verify(token, process.env.APISECRETKEY, (err, decoded) => {
            if (err) {
                res.status(403).json({
                    status: false,
                    message: "Failed to authenticate token!"
                });
            } else {
                if (decoded.user_id) {
                    req.user_id = decoded.user_id;
                    if(req.body.user_id || req.query.user_id){
                        const user_id=req.body.user_id || req.query.user_id;
                        if(decoded.user_id!=user_id){
                            res.status(403).json({
                                status: false,
                                message: "Forbidden! ID mismatched! Please login again!"
                            });
                        }
                    }
                }
                console.log("Decoded verisi:");
                console.log(decoded);
                next();
            }

        });
    } else {  
        res.status(403).json({
            status: false,
            message: "Forbidden! You have to get token!"
        });
    }
};