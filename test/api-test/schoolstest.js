const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const server = require('../../app');

chai.use(chaiHttp);

//Create

describe('Create Sequence', () => {
    let user_id, token, temptoken, passcode, school_id;
    before('(POST /register)', (done) => {
        const testRegisterUser = {
            name: "Ali",
            surname: "Veli",
            email: "aliveli@deli.com",
            password: "av1234"
        };

        chai.request(server)
            .post('/register')
            .send(testRegisterUser)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('success').eql(true);
                res.body.should.have.property('message').eql("Success!");
                done();
            });
    });
    before('(POST /login)', (done) => {
        const testLoginUser = {
            email: "youneedto@enterauser.com",
            password: "sendpassword"
        };

        chai.request(server)
            .post('/login')
            .send(testLoginUser)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('success').eql(true);
                res.body.should.have.property('token');
                res.body.should.have.property('name');
                res.body.should.have.property('surname');
                res.body.should.have.property('user_id');
                user_id = res.body.user_id;
                token = res.body.token;
                done();
            });
    });

    before('(POST /api/schools)', (done) => {
        const testTempSchoolInfo = {
            school_name: "Test ",
            state: "Test",
            city: "Test",
            school_phone: "Test",
            category: "public",
            address: "Test",
            school_email: "test@meb.k12.tr",
            user_id: user_id
        };

        chai.request(server)
            .post('/api/schools')
            .send(testTempSchoolInfo)
            .set('x-access-token', token)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('success').eql(true);
                res.body.should.have.property('temptoken');
                temptoken = res.body.temptoken;
                passcode = res.body.passcode;
                done();
            });
    });
    it('(POST /confirmcreation/verifypasscode)', (done) => {
        const testData = {
            user_id: user_id,
            passcode: passcode
        };

        chai.request(server)
            .post('/confirmcreation/verifypasscode')
            .send(testData)
            .set('x-access-temptoken', temptoken)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('success').eql(true);
                res.body.should.have.property('school_id');
                res.body.should.have.property('schoolName');
                res.body.should.have.property('schoolPhone');
                res.body.should.have.property('schoolEmail');
                school_id = res.body.school_id;
                done();
            });
    });
    describe('Search', () => {

        it('(GET /api/schools)', (done) => {
            chai.request(server)
                .get('/api/schools')
                .query({
                    keyword: 'ulak',
                    state: 'teststate'
                })
                .set('x-access-token', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });

    describe('Update', () => {
        it('(PUT /api/schools)', (done) => {
            const testSchoolInfo = {
                school_id: school_id,
                school_name: "Test",
                state: "Test",
                city: "Test",
                school_phone: "555",
                category: "public",
                address: "Test",
                school_email: "newtestemail@meb.k12.tr",
                user_id: user_id
            };
            chai.request(server)
                .put('/api/schools')
                .send(testSchoolInfo)
                .set('x-access-token', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });
    });

    describe('Delete Sequence', ()=>{
        before('(DELETE /api/schools)', (done)=>{
            const testDeleteInfo={
                user_id:user_id,
                school_id:school_id
            };
            chai.request(server)
            .delete('/api/schools')
            .send(testDeleteInfo)
            .set('x-access-token', token)
            .end((err, res)=>{
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('success').eql(true);
                res.body.should.have.property('temptoken');
                temptoken=res.body.temptoken;
                passcode = res.body.passcode;
                done();
            });
        });
        it('(POST /confirmdeletion/verifypasscode)', (done) => {
            const testConfirmDeleteData = {
                user_id: user_id,
                passcode: passcode
            };
    
            chai.request(server)
                .post('/confirmdeletion/verifypasscode')
                .send(testConfirmDeleteData)
                .set('x-access-temptoken', temptoken)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });
    });

    describe('Delete temp user',()=>{

        it('(DELETE /api/users)', (done) => {
            const testDeleteUser = {
                user_id: user_id,
                password: "av1234"
            };
    
            chai.request(server)
                .delete('/api/users')
                .send(testDeleteUser)
                .set('x-access-token', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });
    });

});