const chai= require('chai');
const chaiHttp= require('chai-http');
const should= chai.should();
const server=require('../../app');

chai.use(chaiHttp);

// Register
describe('Register for user test', ()=>{
    it('(POST /register)', (done)=>{
        const testRegisterUser={
            name:"test name",
            surname:"test surname",
            email: "test@test.com",
            password: "testpassword"
        };

        chai.request(server)
        .post('/register')
        .send(testRegisterUser)
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            done();
        });
    });
});

describe('Login', ()=>{
    let user_id, token;
    before('(POST /login)', (done)=>{
        const testLoginUser={
            email: "test",
            password: "test"
        };

        chai.request(server)
        .post('/login')
        .send(testLoginUser)
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            res.body.should.have.property('token');
            res.body.should.have.property('name');
            res.body.should.have.property('surname');
            res.body.should.have.property('user_id');
            user_id=res.body.user_id;
            token=res.body.token;
            done();
        });
    });

    before('(PUT /api/users/updatemyinfo)', (done)=>{
        const testUpdateInfo={
            name : "Test User Name",
            surname: "test ",
            email: "test@test.com",
            password: "test",
            user_id: user_id
        };
        chai.request(server)
        .put('/api/users/updatemyinfo')
        .send(testUpdateInfo)
        .set('x-access-token', token)
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            done();
        });
    });
    before('(PUT /api/users/changepassword)', (done)=>{
        const testUpdateInfo={
            user_id: user_id,
            password: "test",
            newPassword: "test"
            
        };
        chai.request(server)
        .put('/api/users/changepassword')
        .send(testUpdateInfo)
        .set('x-access-token', token)
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            done();
        });
    });
    before('(PUT /api/users/updatemyphone)', (done)=>{
        const testUpdateInfo={
            user_id: user_id,
            password: "test",
            phone: "555"
            
        };
        chai.request(server)
        .put('/api/users/updatemyphone')
        .send(testUpdateInfo)
        .set('x-access-token', token)
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            done();
        });
    });
    it('(DELETE /api/users)', (done) => {
        const testDeleteUser = {
            user_id: user_id,
            password: "test"
        };

        chai.request(server)
            .delete('/api/users')
            .send(testDeleteUser)
            .set('x-access-token', token)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('success').eql(true);
                done();
            });
    });
});
