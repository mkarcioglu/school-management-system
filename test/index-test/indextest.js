const chai= require('chai');
const chaiHttp= require('chai-http');
const should= chai.should();
const server=require('../../app');

chai.use(chaiHttp);

// Don't forget to uncomment the test line in "user.js" route.
// Important!! Don't forget the comment it again after the test is finished.


//Root
describe('Ulak Index Test', ()=>{
    it('(GET /)  Index Response Test', (done)=>{
        chai.request(server)
        .get('/')
        .end((err, res)=>{
            res.should.have.status(200);
            done();
        });
    });  
});
// Root POST
describe('Ulak Index Test', ()=>{
    it('(POST /)  Index Response Test', (done)=>{
        chai.request(server)
        .post('/')
        .end((err, res)=>{
            res.should.have.status(200);
            done();
        });
    });  
});
//Register
describe('Register', ()=>{
    it('(POST /register)', (done)=>{
        const testRegisterUser={
            name:"Test",
            surname:"test",
            email: "test@test.com",
            password: "testpassword"
        };

        chai.request(server)
        .post('/register')
        .send(testRegisterUser)
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            done();
        });
    });
});

//Login
describe('Login', ()=>{
    it('(POST /login)', (done)=>{
        const testLoginUser={
            email: "test@test.com",
            password: "test"
        };

        chai.request(server)
        .post('/login')
        .send(testLoginUser)
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            res.body.should.have.property('token');
            res.body.should.have.property('name');
            res.body.should.have.property('surname');
            res.body.should.have.property('user_id');
            done();
        });
    });
});

//Continue with fb
describe('Continue With Facebook', ()=>{
    it('(POST /continuewithfb)', (done)=>{
        const testFacebookUser={
            name: "test",
            surname: "test",
            facebook_id: "123456789abcdef",
            password: "test123456"
        };

        chai.request(server)
        .post('/continuewithfb')
        .send(testFacebookUser)
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            res.body.should.have.property('token');
            res.body.should.have.property('name');
            res.body.should.have.property('surname');
            res.body.should.have.property('_id');
            done();
        });
    });
});
//Lost Password
let temptoken, passcode;
describe('Lost Password Sequence', ()=>{
    before('(POST /givemepasscode)', (done)=>{
        const testAlzheimerUser={
            email: "test@test.com"
        };

        chai.request(server)
        .post('/givemepasscode')
        .send(testAlzheimerUser)
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            res.body.should.have.property('temptoken');
            temptoken=res.body.temptoken;
            passcode=res.body.passcode;
            done();
        });
    });
    before('(POST /lost/verifypasscode)', (done)=>{
        const testVfAlzheimerUser={
            email: "test@test.com",
            passcode: passcode
        };

        chai.request(server)
        .post('/lost/verifypasscode')
        .set('x-access-temptoken', temptoken)
        .send(testVfAlzheimerUser)
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            res.body.should.have.property('temptoken');
            temptoken=res.body.temptoken;
            done();
        });
    });
    it('(POST /lost/newpassword)', (done)=>{
        const testNPAlzheimerUser={
            email: "test@test.com",
            password: "test546546"
        };

        chai.request(server)
        .post('/lost/newpassword')
        .set('x-access-temptoken', temptoken)
        .send(testNPAlzheimerUser)
        .end((err, res)=>{
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            done();
        });
    });
});
